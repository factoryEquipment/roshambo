/**
 * 
 */
package com.locomoti.android.games.roshambo.ui.home.fragments;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.locomoti.android.games.roshambo.HomeActivity;
import com.locomoti.android.games.roshambo.R;
import com.locomoti.android.games.roshambo.util.GameConstants;

/**
 * Displays characters for the user to select for when
 * either sending a challenge to a fRriend, or retaliating
 * to a challenge send by a friends
 * 
 * @author 	Sergio Bernales
 * @date 	Mar 12, 2012
 *
 * Copyright 2012 Locomoti LLC. All rights reserved.
 *
 */
public class CharacterSelectionFragment extends Fragment implements OnClickListener {
	private static final String TAG = "CharacterSelectionFragment";
	
	private OnCharacterSelectedListener mListener;
	
	/**
	 * Called when fragment is attached to the Activity. First method
	 * in the fragment lifecycle that is added.
	 * 
	 */
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		
        try {
            mListener = (OnCharacterSelectedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnArticleSelectedListener");
        }
	}
	
	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		// inflate our view
		View v = inflater.inflate(R.layout.fragment_character_selection, container, false);
		
		v.findViewById(R.id.cowboy).setOnClickListener(this);
		v.findViewById(R.id.bear).setOnClickListener(this);
		v.findViewById(R.id.ninja).setOnClickListener(this);
		
		return v;
		
	}
	
	
	 /**
	 * On Click listener that handles one
	 * of the characters being selected
	 */
	@Override
	public void onClick(View v) {

		// check to see that all our required data has been entered by user
		if(!((HomeActivity)getActivity()).isNewGameStepsCompleted()){
			return;
		}

		// character image id that got clicked
		int viewID = v.getId();
		
		// determine which character was clicked
		switch(viewID) {
			case R.id.cowboy:
				mListener.onCharacterSelected(GameConstants.COWBOY_MOVE);
				Log.d(TAG, "cowboy selected");
				break;
			case R.id.bear:
				mListener.onCharacterSelected(GameConstants.BEAR_MOVE);
				Log.d(TAG, "bear selected");
				break;
			case R.id.ninja:
				mListener.onCharacterSelected(GameConstants.NINJA_MOVE);
				Log.d(TAG, "ninja selected");
				break;
			default:
				Log.d(TAG, "nothing selected");
				return;
		}
		
		// show the game summary
		TextView gameSummary = (TextView) getView().findViewById(R.id.game_summary);
		gameSummary.setText(((HomeActivity)getActivity()).getNewGameVO().toString());
		gameSummary.setVisibility(View.VISIBLE);
		gameSummary.invalidate();
		
		// show the go/submit button
		ImageView goImage = (ImageView) getView().findViewById(R.id.go_bubble);
		goImage.setVisibility(View.VISIBLE);
	}
	
	/**
	 * Interface parent activity should implement
	 * to receive notification that a character has been selected
	 * 
	 * @author 	Sergio Bernales
	 * @date 	Mar 26, 2012
	 *
	 * Copyright 2012 Locomoti LLC. All rights reserved.
	 *
	 */
	public interface OnCharacterSelectedListener{
		public void onCharacterSelected(String character);
	}
	
}
