package com.locomoti.android.games.roshambo.ui.home.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import com.locomoti.android.games.roshambo.ui.home.fragments.ActiveGamesFragment;
import com.locomoti.android.games.roshambo.ui.home.fragments.CharacterSelectionFragment;
import com.locomoti.android.games.roshambo.ui.home.fragments.HomeFragment;
import com.locomoti.android.games.roshambo.ui.home.fragments.OpponentSelectionFragment;
import com.locomoti.android.games.roshambo.ui.home.fragments.StakesFragment;

/**
 * Adapter to hold all the fragment required for
 * our home activity view pager.
 * Fragment positions are as follows:
 * 	0 : Open Games Fragment
 *  1 : Home Screen Fragment
 * 	2 : Choose Opponent Fragment
 *	3 : Specify Stakes Fragment
 *    (note: Opponent Analytics would go here for premium version. For now, core flow)
 * 	4 : Choose a character Fragment
 *
 * @author Sergio Bernales
 */
public class HomeFragmentPagerAdapter extends FragmentPagerAdapter {
	
	// Set position for the Home Activity's fragments
	public final static int ACTIVE_GAMES_FRAG_POS = 0;
	public final static int HOME_SCREEN_FRAG_POS = 1;
	public final static int OPP_SEL_FRAG_POS = 2;
	public final static int STAKES_FRAG_POS = 3;
	public final static int CHAR_SEL_FRAG_POS = 4;
		// public final static int OPP_ANALYTICS_FRAG_POS 
	
	
	/** Keep reference to our Fragments so that we can reuse
	 * them if they are still available instead of creating a new
	 * one each time getItme id called*/
	ActiveGamesFragment mActiveGamesFragment;
	HomeFragment mHomeFragment;
	OpponentSelectionFragment mOppSelectionFragment;
	StakesFragment mStakesFragment;
	CharacterSelectionFragment mCharSelectionFragment;
	
	Context mContext;
	ViewPager mViewPager; 
	
	public HomeFragmentPagerAdapter(Activity activity, ViewPager pager) {
		// link with the respective fragment manager
		super( ((FragmentActivity)activity).getSupportFragmentManager());
		
		mContext = activity;
		mViewPager = pager;
		
	}

	
	/**
	 * Return a new fragment. For now its just the home fragment
	 */
	@Override
	public Fragment getItem(int position) {
		
		switch(position){
		case HomeFragmentPagerAdapter.ACTIVE_GAMES_FRAG_POS:
			if (mActiveGamesFragment == null)
				mActiveGamesFragment = new ActiveGamesFragment(); 
			return mActiveGamesFragment;
			
		case HomeFragmentPagerAdapter.HOME_SCREEN_FRAG_POS:
			if(mHomeFragment == null)
				mHomeFragment = new HomeFragment();
			return mHomeFragment;
			
		case HomeFragmentPagerAdapter.OPP_SEL_FRAG_POS:
			if(mOppSelectionFragment == null)
				mOppSelectionFragment = new OpponentSelectionFragment();
			return mOppSelectionFragment;
			
		case HomeFragmentPagerAdapter.STAKES_FRAG_POS:
			if(mStakesFragment == null)
				mStakesFragment = new StakesFragment();
			return mStakesFragment;
			
		case HomeFragmentPagerAdapter.CHAR_SEL_FRAG_POS:
			if(mCharSelectionFragment == null)
				mCharSelectionFragment = new CharacterSelectionFragment();
			return mCharSelectionFragment;
			
		default:
			return null;
		}
	}


	@Override
	public int getCount() {
		return 5;
	}

}
