/**
 * 
 */
package com.locomoti.android.games.roshambo.ui.home.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.locomoti.android.games.roshambo.R;

/**
 * The main/first fragment that is displayed to the user
 * upon opening the app. This fragment belongs in the 
 * Home Activity View Pager (it's the second fragment)
 * 
 * @author 	Sergio Bernales
 * @date 	Mar 12, 2012
 * 
 * Copyright 2012 Locomoti LLC. All rights reserved.
 *
 */
public class HomeFragment extends Fragment {
	private static final String TAG  ="HomeFragment";
	
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	/**
	 * Inflate the Home View. This first thing user ever sees!
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View v = inflater.inflate(R.layout.fragment_home, container,false);
		return v;
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	
	/**
	 *  Home Fragment is about to get shown. Start up animations again
	 */
	@Override
	public void onStart() {
		Animation a = AnimationUtils.loadAnimation(getActivity(), R.anim.right_arrow_animator);
		
		View arrow = getView().findViewById(R.id.right_arrow);
		arrow.startAnimation(a);
//		
		super.onStart();
	}
	
	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onActivityCreated(android.os.Bundle)
	 */
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}
	
	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onSaveInstanceState(android.os.Bundle)
	 */
	@Override
	public void onSaveInstanceState(Bundle outState) {
		 
//		called when activity has stopped
		super.onSaveInstanceState(outState);
		
		Log.d(TAG, " saving the state");
	}
	
	/**
	 * Called when fragment looses focus. When
	 * the fragment is swiped 2 away.
	 */
	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		Log.d(TAG, " on stop");
		super.onStop();
	}
}
