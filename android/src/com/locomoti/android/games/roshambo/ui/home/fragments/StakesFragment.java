/**
 * 
 */
package com.locomoti.android.games.roshambo.ui.home.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import com.locomoti.android.games.roshambo.HomeActivity.OnModeChangedListener;
import com.locomoti.android.games.roshambo.R;

/**
 * TODO: click anywhere to hide keyboard
 * 
 * Fragment that asks the user what the stakes are
 * in the challenge that they are about to initiate.
 * 
 * At this point GAME_MODE should be past MODE_INITIAL.
 * The user should have already selected an opponent
 * 
 * @author 	Sergio Bernales
 * @date 	Mar 23, 2012
 *
 * Copyright 2012 Locomoti LLC. All rights reserved.
 *
 */
public class StakesFragment extends Fragment implements OnTouchListener, OnModeChangedListener {
	private static final String TAG = "StakesFragment";
	
	AutoCompleteTextView mTextBox;
	
	/**
	 * 
	 */
	public StakesFragment() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Create the Enter Stakes screen so the user can type
	 * in what the battle is about 
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		if (container == null) {
            // We have different layouts, and in one of them this
            // fragment's containing frame doesn't exist.  The fragment
            // may still be created from its saved state, but there is
            // no reason to try to create its view hierarchy because it
            // won't be displayed.  Note this is not needed -- we could
            // just run the code below, where we would create and return
            // the view hierarchy; it would just never be used.
            return null;
        }
		

		
		// inflate our view
		View v = inflater.inflate(R.layout.fragment_stakes, container, false);

		// set the id in order to retrieve later by the Fragment Manager
//		v.setId(R.id.stakes_fragment);
		
		/** Auto complete test attributes TODO*/
		AutoCompleteTextView mTextBox = (AutoCompleteTextView) v.findViewById(R.id.stakes);
		
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_dropdown_item_1line, STAKES_SUGGESTIONS);
		mTextBox.setAdapter(adapter);
		mTextBox.setOnTouchListener(this);
		
		return v;
	}
	
	private static final String[] STAKES_SUGGESTIONS = new String[] {
        "Belgium", "France", "Italy", "Germany", "Spain", "bolivia"
    };

	/* (non-Javadoc)
	 * @see android.view.View.OnClickListener#onClick(android.view.View)
	 */
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		((AutoCompleteTextView)v).showDropDown();
		
		return false;
	}

	/**
	 * Mode has changed in the Home Activity
	 * Usually for the stakes fragment means that
	 * an opponenet has been selected
	 */
	@Override
	public void onModeChanged(int newMode) {
		Log.d(TAG, "Hi from stakesfragment listener");
		
	}

	/**
	 * Returns the stakes the user entered into the form.
	 * 
	 * @return
	 */
	public String getStakes() {
		// TODO: this causes a lot of force closes , maybe its the textview or maybe in OnPageSelected, it happens when resuming the app
		AutoCompleteTextView mTextBox = (AutoCompleteTextView) getView().findViewById(R.id.stakes);
		return mTextBox.getText().toString();
	}
	
	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onDestroyView()
	 */
	@Override
	public void onDestroyView() {
		// TODO Auto-generated method stub
		super.onDestroyView();
	}
}
