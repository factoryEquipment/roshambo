/**
 * 
 */
package com.locomoti.android.games.roshambo;

/**
 * @author 	Sergio Bernales
 * @date 	Apr 6, 2012
 *
 * Copyright 2012 Locomoti LLC. All rights reserved.
 *
 */
public class C2DMConfig {
    /**
     * Cloud sender - replace with your one email, should match the account
     * used for authentication by the cloud sender.
     */
    public static final String C2DM_SENDER = "locomoti.mobile@gmail.com";
    public static final String C2DM_EXTRA_STAKES = "stakes";
    public static final String C2DM_EXTRA_GAME_ID = "gameid";
    public static final String C2DM_EXTRA_CHA_USERNAME = "chausername";
}
