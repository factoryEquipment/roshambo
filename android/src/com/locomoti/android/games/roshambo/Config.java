/**
 * 
 */
package com.locomoti.android.games.roshambo;

/**
 * @author 	Sergio Bernales
 * @date 	Apr 6, 2012
 *
 * Copyright 2012 Locomoti LLC. All rights reserved.
 *
 */
public class Config {
    /**
     * Cloud sender - replace with your one email, should match the account
     * used for authentication by the cloud sender.
     */
    public static final String C2DM_SENDER = "locomoti.mobile@gmail.com";
    public static final String C2DM_ACCOUNT_EXTRA = "account_name";
    public static final String C2DM_MESSAGE_EXTRA = "message";
    public static final String C2DM_MESSAGE_SYNC = "sync";
}
