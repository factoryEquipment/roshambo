/**
 * 
 */
package com.locomoti.android.games.roshambo.intro.ui;

import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.c2dm.C2DMConfig;
import com.google.android.c2dm.C2DMessaging;
import com.locomoti.android.games.roshambo.R;
import com.locomoti.android.games.roshambo.home.ui.HomeActivity;
import com.locomoti.android.games.roshambo.provider.RoshamboAsyncQueryHandler;
import com.locomoti.android.games.roshambo.provider.RoshamboAsyncQueryHandler.AsyncQueryListener;
import com.locomoti.android.games.roshambo.provider.RoshamboContract.User;

/**
 * When starting the app for the first time, this activity will launch.
 * It will just create the user and also ask for a name and email they
 * want to connect to the account.
 * Here we use two Async query methodologies.
 * 
 * The Loader: we use this when we query for the user. This solves the problem
 * that if the user does not exist and is being created, instead of querying
 * the DB to check if it's done OR block our worker thread and deal with the complexity
 * if the worker thread is done or not, we let the loader take care
 * of observing for content changes
 * 
 * The AsyncQueryHandler: we use this to actually insert our new user. This will
 * create a new user record as well as ask the server to generate a new user id. 
 * This happens while our Loader is still quering our User table waiting for the
 * USER_ID to be populated (thats when we know the server has generated and user id
 * and returned it to us)
 * 
 * TODO: make sure to remove this activity from the activity stack after leaving
 * 
 * @author 	Sergio Bernales
 * @date 	Mar 16, 2012
 *
 * Copyright 2012 Locomoti LLC. All rights reserved.
 *
 */
public class IntroActivity extends FragmentActivity implements AsyncQueryListener, 
						OnClickListener, OnSharedPreferenceChangeListener{
	private static final String TAG = "IntroActivity";
	
	private final static int USER_EXISTS_TASK = 0;// TODO: can this be the same number as loaders in other activities?
	private final static int USER_CREATE_TASK = 1;// TODO: can this be the same number as loaders in other activities?
	private final static int USER_UPDATE_TASK = 2;// TODO: can this be the same number as loaders in other activities?

	private RoshamboAsyncQueryHandler mAsyncQueryHandler;
	private UserObserver mObserver; 
	
	private Button mLoginButton;
	private View mLoginView;
	private TextView mEmailTextBox;
	private TextView mUsernameTextBox;
	
	/**
	 * Entry point of the app at first launch
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_intro);
		 
		// setup the login button TODO: sepearete register login button
		mLoginButton = (Button) findViewById(R.id.login_button);
		mLoginButton.setOnClickListener(this);
		
		// reference the login view (sowe can show later if needed)
		mLoginView = findViewById(R.id.login_view);
		
		//reference user data textboxes
		mEmailTextBox = (TextView) findViewById(R.id.email);
		mUsernameTextBox = (TextView) findViewById(R.id.username);
		
		// Reference to async query object 
		mAsyncQueryHandler = new RoshamboAsyncQueryHandler(getContentResolver(), this);
		
		// Query to see if user exists
		mAsyncQueryHandler.startQuery(USER_EXISTS_TASK, null, User.buildUserUri(User.DEFAULT_ID), 
				UserExistsQuery.PROJECTION, UserExistsQuery.SELECTION, null, null);
		
		/** Performance suggestions per ANdroid javadocs HttpURLConnection*/
		System.setProperty("http.keepAlive", "false");//TODO https.keepalive?
	}
	
	/**
	 * Process async query callback here. The Check is user existsw query.
	 */
	@Override
	public void onQueryComplete(int token, Object cookie, Cursor cursor) {
		if(cursor == null )
			return;
		
		// We do not have a user created, and never began to create it
		if(cursor == null || cursor.getCount() <= 0){
			
			Toast t = Toast.makeText(this, "User does not exist, create one?", 5000);
			t.show();
			
			// create the obersever that will get notified when the user is finally created
			if (mObserver == null)
				mObserver = new UserObserver(this, new Handler());
			
			// register said observer to listen to 
			getContentResolver().registerContentObserver(User.CONTENT_URI, true, mObserver);//TODO: remove this ocntent observer in OnPause
			
			// Display the login view to get user credentials
			mLoginView.setVisibility(View.VISIBLE);
			
			return;
		}
			
		// check that the  device is registered with c2dm google server
		if(!c2dmRegistered()){
			// if not, register with itregister with the c2dm google server
			C2DMessaging.register(this, C2DMConfig.C2DM_SENDER);
			return;
		}
		
		
		// we have all user data
		Toast.makeText(this, "User Exists!!! :)", 5000).show();
		// Otherwise user exists, lets proceed to home page :)
		startActivity(new  Intent(this, HomeActivity.class));
		finish();
	}
	

	/**
	 * Checks to see that the device is registered with the c2dm
	 * google server in order to receive notifications
	 * @return
	 */
	private boolean c2dmRegistered() {
		// gets registration id rom preferences
		String regId = C2DMessaging.getRegistrationId(this);
		
		// if not null and is not empty
		return (regId != null) && !(TextUtils.isEmpty(regId));
	}

	/**
	 * Process async insert callback
	 * 
	 * TODO: if user not created successfully, try again
	 */
	@Override
	public void onInsertComplete(int token, Object cookie, Uri uri ){
		Toast t = Toast.makeText(this, "Created user: " + token, 5000);
		t.show();
	}
	
	/* (non-Javadoc)
	 * @see com.locomoti.android.games.roshambo.provider.RoshamboAsyncQueryHandler.AsyncQueryListener#onUpdateComplete(int, java.lang.Object, android.net.Uri)
	 */
	@Override
	public void onUpdateComplete(int token, Object cookie, int result) {
		Toast t = Toast.makeText(this, "Updated user: " + token, 5000);
		t.show();
	}
	
	/**
	 * See if a user exists for this device
	 *
	 */
    private interface UserExistsQuery {
        String[] PROJECTION = {
            User._ID,
            User.USER_NODE_ID, // id from the server
            User.USER_USERNAME
        };
        
        String SELECTION = User.USER_NODE_ID + " IS NOT NULL";

        int _ID = 0;
        int USER_ID = 1;
    }

	/**
	 * Listener for the tap on the Login button.
	 * Should first register with C2DM so that we send
	 * the c2dm registration ID at the same time
	 * as the user stuff
	 */
	@Override
	public void onClick(View v) {
		Toast t = Toast.makeText(this, "You clicked me!", 5000);
		t.show();
		
		// check that the  device is registered with c2dm google server
		if(!c2dmRegistered()){
			// if not, register with itregister with the c2dm google server
			// register with the c2dm google server
			C2DMessaging.setPreferenceOnChangeListener(this);
			C2DMessaging.register(this, C2DMConfig.C2DM_SENDER);
			return;
		}
		
		// Otherwise user exists, lets proceed to home page :)
		// Setup our user values to send to our Content Provider
		ContentValues values = new ContentValues();
		values.put(User.USER_USERNAME, mUsernameTextBox.getText().toString());
		values.put(User.USER_EMAIL, mEmailTextBox.getText().toString());
		values.put(User._ID, User.DEFAULT_ID);
		
		// send insert command
		mAsyncQueryHandler.startInsert(USER_CREATE_TASK, null, User.CONTENT_URI, values);
	}

	/**
	 * Will observe the user table. Waiting that the user gets created.
	 * Until then we wills stay in the intro page.
	 * 
	 * @author 	Sergio Bernales
	 * @date 	Mar 19, 2012
	 *
	 * Copyright 2012 Locomoti LLC. All rights reserved.
	 *
	 */
	private class UserObserver extends ContentObserver {

		Context mContext;
		/**
		 * @param handler
		 */
		public UserObserver(Context context, Handler handler) {
			super(handler);
			
			mContext = context;
		}
		
		/**
		 * metod that gets called after the user has been created 
		 * in the server. Proceed to next activity
		 * 
		 * @see android.database.ContentObserver#onChange(boolean)
		 */
		@Override
		public void onChange(boolean selfChange) {
			Log.d(TAG, "We got notified");
			Toast t = Toast.makeText(IntroActivity.this, "Some one updated the user!!!", 5000);
			t.show();	
			
			startActivity(new  Intent(mContext, HomeActivity.class));
			finish();
		}

		@Override
		public boolean deliverSelfNotifications() {
			return true;
		}	
	}
	
	
	
	
	/* (non-Javadoc)
	 * @see android.support.v4.app.FragmentActivity#onStop()
	 */
	@Override
	protected void onStop() {
		super.onStop();
		
		// clear our callback as we dont need the response anymore!is 
		mAsyncQueryHandler.clearQueryListener();
		
		// remove the observer
		if (mObserver != null)
			getContentResolver().unregisterContentObserver(mObserver);
	}

	/**
	 * Listens to a change of the registration id preference from the C2DM 
	 * registration process. Once we have this ID we can send all user
	 * info to the server to complete user creation
	 */
	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,
			String key) {
		
		if (key.equalsIgnoreCase("dm_registration")){
			// Setup our user values to send to our Content Provider
			ContentValues values = new ContentValues();
			values.put(User.USER_USERNAME, mUsernameTextBox.getText().toString());
			values.put(User.USER_EMAIL, mEmailTextBox.getText().toString());
			values.put(User._ID, User.DEFAULT_ID);
			
			// unregister from listening to preference
			C2DMessaging.unsetPreferenceOnChangeListener(this);
			
			// send insert command
			mAsyncQueryHandler.startInsert(USER_CREATE_TASK, null, User.CONTENT_URI, values);
		}
	}
}
