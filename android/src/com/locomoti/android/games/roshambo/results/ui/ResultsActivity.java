/**
 * 
 */
package com.locomoti.android.games.roshambo.results.ui;

import android.support.v4.app.FragmentActivity;

/**
 * A viewPager that will let the user flip through
 * the comic srip of the result of the match. Like a 
 * win a tie or anything in between.
 * 
 * @author 	Sergio Bernales
 * @date 	Apr 16, 2012
 *
 * Copyright 2012 Locomoti LLC. All rights reserved.
 *
 */
public class ResultsActivity extends FragmentActivity {

		
}
