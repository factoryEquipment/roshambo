/**
 * 
 */
package com.locomoti.android.games.roshambo.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Broadcast Receiver to implement Cloud 2 Device Messages.
 * This receiver will receive push noitifications of app activity
 * like for example if someone challenge me, I will receive a push onitification
 * that I have a pending challenge. 
 * This receiver is also used to receive the registration ID when registering
 * with the Google C2DM server as this registration id is what we
 * send to out node server. The node server will use the registration ID
 * to send notification request to the C2DM servers which will complete the full circle
 * and send the notification here.
 * 
 * @author 	Sergio Bernales
 * @date 	Apr 6, 2012
 *
 * Copyright 2012 Locomoti LLC. All rights reserved.
 *
 */
public class C2DMBroadcastReceiver extends BroadcastReceiver {
	static final public String TAG = "C2DMReceiver";
	
	/**
	 * Receives the intents from the Android C2DM android service.
	 * It could etiehr be a registration intent or a push 
	 * notification intent
	 */
	@Override
	public void onReceive(Context context, Intent intent) {
	    if (intent.getAction().equals("com.google.android.c2dm.intent.REGISTRATION")) {
	        handleRegistration(context, intent);
	    } else if (intent.getAction().equals("com.google.android.c2dm.intent.RECEIVE")) {
	        handleMessage(context, intent);
	     }
	 }

	/**
	 * Handle push notification intent. This means we have received
	 * a challenge or there are results available for our viewing pleasure.
	 * 
	 * @param context
	 * @param intent
	 */
	private void handleMessage(Context context, Intent intent) {
		Log.d(TAG, "Message came in buddy");
		
	}

	/**
	 * Handle REGISTRATION intent. It could either be a new registration ID
	 * or Google is refreshing their registration ID's as they do
	 * periodically. We check for errors as well.
	 * Then we need to notify the node server of this registration id.
	 * 
	 * @param context
	 * @param intent
	 */
	private void handleRegistration(Context context, Intent intent) {
		Log.d(TAG, "Registration id came in buddyx");
		
	}

}
