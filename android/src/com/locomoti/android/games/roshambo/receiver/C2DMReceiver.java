/**
 * 
 */
package com.locomoti.android.games.roshambo.receiver;

import com.google.android.c2dm.C2DMBaseReceiver;
import com.google.android.c2dm.C2DMConfig;
import com.locomoti.android.games.roshambo.R;
import com.locomoti.android.games.roshambo.challenge.ui.ChallengeReceivedActivity;
import com.locomoti.android.games.roshambo.home.ui.HomeActivity;
import com.locomoti.android.games.roshambo.provider.RoshamboAsyncQueryHandler;
import com.locomoti.android.games.roshambo.provider.RoshamboAsyncQueryHandler.AsyncQueryListener;
import com.locomoti.android.games.roshambo.provider.RoshamboContract.Game;
import com.locomoti.android.games.roshambo.vo.ChallengeReceivedVO;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

/**
 * Broadcast Receiver to implement Cloud 2 Device Messages.
 * This receiver will receive push noitifications of app activity
 * like for example if someone challenge me, I will receive a push onitification
 * that I have a pending challenge. 
 * This receiver is also used to receive the registration ID when registering
 * with the Google C2DM server as this registration id is what we
 * send to out node server. The node server will use the registration ID
 * to send notification request to the C2DM servers which will complete the full circle
 * and send the notification here.
 * 
 * @author 	Sergio Bernales
 * @date 	Apr 6, 2012
 *
 * Copyright 2012 Locomoti LLC. All rights reserved.
 *
 */
public class C2DMReceiver extends C2DMBaseReceiver {
	static final public String TAG = "C2DMReceiver";
	
	static final private String EXTRA_TOKEN = "token";
	static final public int TOKEN_CHALLENGE = '0';
	static final public int TOKEN_TIE = '1';
	static final public int TOKEN_LOSE = '2';
	static final public int TOKEN_WIN = '3';
	
    public C2DMReceiver() {
        super(C2DMConfig.C2DM_SENDER);
    }
	
	/* (non-Javadoc)
	 * @see com.google.android.c2dm.C2DMBaseReceiver#onMessage(android.content.Context, android.content.Intent)
	 */
	@Override
	protected void onMessage(Context context, Intent intent) {
		Log.d(TAG, "We have received a message");

		// get the extras in the message
	    Bundle extras = intent.getExtras();
	    
	    // get the token that will give us context on what kind of meessage we received
	    int token = extras.getInt(EXTRA_TOKEN);
	    
	    switch(token) {
	    	case TOKEN_CHALLENGE:
	    		receiveChallenge(context, extras);
	    		break;
	    		
	    	case TOKEN_LOSE:
	    	case TOKEN_WIN:
	    	case TOKEN_TIE:
	    		Log.d(TAG, "A winner has been chosen");
	    		break;
	    		
	    	default:
	    		break;
	    }
	    

//    	mAsyncQUeryHandler.startInsert(TOKEN_SUBMIT_GAME, mNewGameVO, Game.buildNewGameURI(), mNewGameValues);
	}

	/**
	 * Processes the message containing info about the challenge we just received
	 * @param context 
	 * @param extras 
	 */
	private void receiveChallenge(Context context, Bundle extras) {
	    // parse them into a contentValu object
		ContentValues challengeReceivedValues = ChallengeReceivedVO.toContentValues(extras);

		/**
		 * Here we cross from the realm of json to android app. COntent values used frokm now on
		 */
		
		// create Notification
		Notification notification = createNewChallengeNotification(challengeReceivedValues, context);

    	// put notification
    	String ns = Context.NOTIFICATION_SERVICE;
    	NotificationManager mNotificationManager = (NotificationManager) getSystemService(ns);

    	mNotificationManager.notify(R.string.incoming_challenge, notification);
	}

	/**
	 * @param challengeReceivedValues 
	 * @param context 
	 * @return 
	 * 
	 */
	private Notification createNewChallengeNotification(ContentValues challengeReceivedValues, Context context) {
	    String stakes = challengeReceivedValues.getAsString(Game.GAME_STAKES);
	    String oppUsername= challengeReceivedValues.getAsString(Game.GAME_OPP_USERNAME);	    
	    

    	// text that will tick in the beginning of notification
    	CharSequence tickerText = "Fight for "+stakes;
    	long when = System.currentTimeMillis();

    	// create new notification object
    	Notification notification = new Notification(R.drawable.status_icon, tickerText, when);

    	// text to display in notification bar
    	CharSequence contentTitle = oppUsername + " challenged you";
    	CharSequence contentText = stakes;

    	// intent that will trigger when user taps on notification
    	Intent notificationIntent = new Intent(this, ChallengeReceivedActivity.class);
    	notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_SINGLE_TOP);
    	
    	// copy the extras from our message intent
    	notificationIntent.putExtra(ChallengeReceivedActivity.EXTRA_CHALLENGE_VALUES,	challengeReceivedValues);
    	
    	//create the pending intent and add our challenge values
    	PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, Notification.FLAG_AUTO_CANCEL);
 
    	// update notification with our new content
    	notification.setLatestEventInfo(context, contentTitle, contentText, pendingIntent);
    	
    	// notifrication flags
//    	notification.flags = Notification.DEFAULT_LIGHTS | Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE;


		return notification;
	}

	/* (non-Javadoc)
	 * @see com.google.android.c2dm.C2DMBaseReceiver#onError(android.content.Context, java.lang.String)
	 */
	@Override
	public void onError(Context context, String errorId) {
		// TODO Auto-generated method stub
		
	}
}
