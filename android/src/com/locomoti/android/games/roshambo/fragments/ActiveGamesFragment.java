/**
 * 
 */
package com.locomoti.android.games.roshambo.fragments;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract.Contacts;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Toast;

import com.locomoti.android.games.roshambo.R;
import com.locomoti.android.games.roshambo.provider.RoshamboContract.Game;
import com.locomoti.android.games.roshambo.provider.RoshamboContract.Matches;
import com.locomoti.android.games.roshambo.provider.RoshamboContract.QualifiedColumns;

/**
 * TODO: update the list of games immediatley, refresh it
 * 
 * Holds a list of games. 
 * Games where you have been challenged and
 * you need to submit a character (at the top of the list) ,
 * Or you may have challenged someone and their move is pending (bottom of the list)
 * 
 * @author 	Sergio Bernales
 * @date 	Mar 12,  = 012
 * 
 * Copyright 2012 Locomoti LLC. All rights reserved.
 *
 */

public class ActiveGamesFragment extends ListFragment implements LoaderManager.LoaderCallbacks<Cursor>,
	AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener{
	private static final String TAG = "ActiveGamesFragment";
	
	private final static int LOADER_ACTIVE_GAMES = 0;
	
	SimpleCursorAdapter mCursorAdapter;
	CursorLoader mCursorLoader;
	
	/**
	 * Inflate our active game list view.
	 * 
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		if (container == null) {
            // We have different layouts, and in one of them this
            // fragment's containing frame doesn't exist.  The fragment
            // may still be created from its saved state, but there is
            // no reason to try to create its view hierarchy because it
            // won't be displayed.  Note this is not needed -- we could
            // just run the code below, where we would create and return
            // the view hierarchy; it would just never be used.
            return null;
        }
		
		View v = inflater.inflate(R.layout.fragment_active_games, container, false);
		return v;
	}

	/**
	 * Will add the cursor to this method. The cursor is managed by the activity
	 * so we need to ask for it
	 * 
	 * @see android.support.v4.app.Fragment#onActivityCreated(android.os.Bundle)
	 */
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		// create an empty adapter to put while we query the db
		mCursorAdapter = new SimpleCursorAdapter(getActivity(), 
				android.R.layout.simple_list_item_2,// my row layout.xml
				null, //the cursor (null for now)
				ActiveGamesQuery.CURSOR_PROJECTION,// from
				new int[] { android.R.id.text1, android.R.id.text2 }, // to
				0); //flags (none)
		
		setListAdapter(mCursorAdapter);
		
		LoaderManager.enableDebugLogging(true);
		
		
		// Prepare the loader.  Either re-connect with an existing one,
        // or start a new one.
		getLoaderManager().initLoader(LOADER_ACTIVE_GAMES, null, this);
	}

	
	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onStart()
	 */
	@Override
	public void onStart() {
		super.onStart();
		
		// force load loader as notifying change NEVER WORKED!
		mCursorLoader.forceLoad(); //TODO: this could be called from HomeActivity
	}
	
	/**
	 * Called when a new loader needs to be created. Here we create the query that will return us,
	 * asynchronously, the list of active games.
	 * 
	 * @see android.support.v4.app.LoaderManager.LoaderCallbacks#onCreateLoader(int, android.os.Bundle)
	 */
	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		switch (id){
			case LOADER_ACTIVE_GAMES: {
				mCursorLoader = new MyCursorLoader(getActivity(), Game.buildActiveGamesURI(),
						ActiveGamesQuery.QUERY_ROJECTION, null, null,
		                null );
				
				return mCursorLoader;
				
			}
			default: {
				throw new IllegalArgumentException("We did not load that loader");
			}
		}
	}
	
	public static class MyCursorLoader extends CursorLoader {

		/**
		 * @param context
		 * @param uri
		 * @param projection
		 * @param selection
		 * @param selectionArgs
		 * @param sortOrder
		 */
		public MyCursorLoader(Context context, Uri uri, String[] projection,
				String selection, String[] selectionArgs, String sortOrder) {
			super(context, uri, projection, selection, selectionArgs, sortOrder);
			// TODO Auto-generated constructor stub
		}
		
		/* (non-Javadoc)
		 * @see android.support.v4.content.CursorLoader#loadInBackground()
		 */
		@Override
		public Cursor loadInBackground() {
			// TODO Auto-generated method stub
			
			return super.loadInBackground();
		}
		
		/* (non-Javadoc)
		 * @see android.support.v4.content.Loader#onContentChanged()
		 */
		@Override
		public void onContentChanged() {
			// TODO Auto-generated method stub
			super.onContentChanged();
		}
		
		
	}
	
	/**
	 * Here we swap in the new cursor. CLosing will be handled by the actual fragment
	 * 
	 * @see android.support.v4.app.LoaderManager.LoaderCallbacks#onLoadFinished(android.support.v4.content.Loader, java.lang.Object)
	 */
	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
		Log.d(TAG, Integer.toString(data.getCount()));
		mCursorAdapter.swapCursor(data);
		
		
		getListView().setOnItemClickListener(this);
		getListView().setOnItemLongClickListener(this);
	}

	/**
	 * Called when the last cursor sent to OnLoadFinished is about to be closed. We just
	 * swap for a null cursor. No damage done.
	 * 
	 * @see android.support.v4.app.LoaderManager.LoaderCallbacks#onLoaderReset(android.support.v4.content.Loader)
	 */
	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
		mCursorAdapter.swapCursor(null);
	}
	
	
	/**
	 * The query to retrieve active games. It should inner join
	 * the latest match move and sort by first my_move or their_move
	 * and then by creation date.
	 *  
	 */
    private interface ActiveGamesQuery {
        String[] QUERY_ROJECTION = {
            Game.GAME_STAKES,
            Game.GAME_OPP_NAME,
    		Matches.GAME_REF_ID + " as _id",
            Game.GAME_NODE_ID
        };
        
        String[] CURSOR_PROJECTION = {
            Game.GAME_STAKES,
            Game.GAME_OPP_NAME,
    		Game._ID,
            Game.GAME_NODE_ID
        };
        
        int GAME_STAKES = 0;
        int GAME_OPP_NAME = 1;
        int _ID = 2;
        int GAME_NODE_ID = 3;
    }


	/* (non-Javadoc)
	 * @see android.widget.AdapterView.OnItemLongClickListener#onItemLongClick(android.widget.AdapterView, android.view.View, int, long)
	 */
	@Override
	public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
		Toast t = Toast.makeText(getActivity(), "onl ong click", 2000);
		t.show();
		return true;
	}

	/* (non-Javadoc)
	 * @see android.widget.AdapterView.OnItemClickListener#onItemClick(android.widget.AdapterView, android.view.View, int, long)
	 */
	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		Toast t = Toast.makeText(getActivity(), "query completed!", 5000);
		t.show();
		
	}
}
