/**
 * 
 */
package com.locomoti.android.games.roshambo.fragments;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.AutoCompleteTextView;

/**
 * Just an extension of the AutoCompleteTextView
 * so that it shows the drop down as soon as the user
 * enter the textbox
 * 
 * @author 	Sergio Bernales
 * @date 	Mar 23, 2012
 *
 * Copyright 2012 Locomoti LLC. All rights reserved.
 *
 */
public class ImmediateAutoComplete extends AutoCompleteTextView {

	

	
	/**
	 * @param context
	 * @param attrs
	 * @param defStyle
	 */
	public ImmediateAutoComplete(Context context, AttributeSet attrs,
			int defStyle) {
		super(context, attrs, defStyle);
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * @param context
	 * @param attrs
	 * @param defStyle
	 */
	public ImmediateAutoComplete(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		
		
	}
	
	/**
	 * @param context
	 * @param attrs
	 * @param defStyle
	 */
	public ImmediateAutoComplete(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Always return true!
	 */
	@Override
	public boolean enoughToFilter() {
		// TODO Auto-generated method stub
		return true;
	}
}
