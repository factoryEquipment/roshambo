/**
 * 
 */
package com.locomoti.android.games.roshambo.fragments;

import android.support.v4.app.DialogFragment;

/**
 * After all steps of the challenge creation process
 * has been completed, this dialog will display a 
 * confirmation of challenge dialog box that the user will be able to approve
 * or unapprove to go back and update some of the game data
 * 
 * @author 	Sergio Bernales
 * @date 	Mar 27, 2012
 *
 * Copyright 2012 Locomoti LLC. All rights reserved.
 *
 */
public class SubmitChallengeFragment extends DialogFragment {
//	
//	int mNum = 1;
//	/* (non-Javadoc)
//	 * @see android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)
//	 */
//	@Override
//	public View onCreateView(LayoutInflater inflater, ViewGroup container,
//			Bundle savedInstanceState) {
//	      View v = inflater.inflate(R.layout.fragment_dialog, container, false);
//	        View tv = v.findViewById(R.id.text);
//	        ((TextView)tv).setText("Dialog #" + mNum + ": using style "
//	                + getNameForNum(mNum));
//
//	        // Watch for button clicks.
//	        Button button = (Button)v.findViewById(R.id.show);
//	        button.setOnClickListener(new OnClickListener() {
//	            public void onClick(View v) {
//	                // When button is clicked, call up to owning activity.
//	                ((FragmentDialog)getActivity()).showDialog();
//	            }
//	        });
//
//	        return v;
//
//	}

}
