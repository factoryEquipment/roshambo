/**
 * 
 */
package com.locomoti.android.games.roshambo.fragments;

import com.locomoti.android.games.roshambo.R;

import android.app.Activity;
import android.content.ContentValues;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * @author 	Sergio Bernales
 * @date 	Apr 16, 2012
 *
 * Copyright 2012 Locomoti LLC. All rights reserved.
 *
 */
public class GameInfoFragment extends Fragment {

	ContentValues mGameInfoValues;
	
	
	/**
	 * 
	 */
	public GameInfoFragment(ContentValues gameInfo) {
		mGameInfoValues = gameInfo;
	}
	
	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onAttach(android.app.Activity)
	 */
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
	}
	
	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
	
		// inflate our view
		View v = inflater.inflate(R.layout.fragment_game_info, container, false);
		
		return v;
	}

}
