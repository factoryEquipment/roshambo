/**
 * 
 */
package com.locomoti.android.games.roshambo.fragments;

import android.app.Activity;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.Contacts;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.telephony.PhoneNumberUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.locomoti.android.games.roshambo.R;

/**
 * Displays a list of contacts available to challenge to a duel!
 * For now we are reading only from the Contact on the user's phone.
 * Soon, we will aggregate Facebook friends. Then maybe twitter.
 * 
 * 
 * @author 	Sergio Bernales
 * @date 	Mar 12, 2012
 *
 * Copyright 2012 Locomoti LLC. All rights reserved.
 *
 */
public class OpponentSelectionFragment extends ListFragment implements LoaderManager.LoaderCallbacks<Cursor> {
	private static final String TAG = "OpponentSelectionFragment";
	
	SimpleCursorAdapter mCursorAdapter;
	
	OnOpponentSelectedListener mListener;
	
	/** Loaders this fragment will interface with */
	private static final int LOADER_CONTACTS_W_NUMBERS = 20;
	
	/**
	 * Called when fragment is attached to the Activity. First method
	 * in the fragment lifecycle that is added.
	 * 
	 */
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		
        try {
            mListener = (OnOpponentSelectedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnArticleSelectedListener");
        }
	}
	
	/**
	 * Inflate our active game list view.
	 * 
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		
		if (container == null) {
            // We have different layouts, and in one of them this
            // fragment's containing frame doesn't exist.  The fragment
            // may still be created from its saved state, but there is
            // no reason to try to create its view hierarchy because it
            // won't be displayed.  Note this is not needed -- we could
            // just run the code below, where we would create and return
            // the view hierarchy; it would just never be used.
            return null;
        }
		
		// inlfate our view
		View v = inflater.inflate(R.layout.fragment_opponent_selection, container, false);
		
		// set the id in order to retrieve later by the Fragment Manager
//		v.setId(R.id.opponent_selection_fragment);
		
		return v;
	}

	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onActivityCreated(android.os.Bundle)
	 */
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		
		// Create an empty adapter that we will modify after our query returns
		mCursorAdapter = new SimpleCursorAdapter(getActivity(), 
										android.R.layout.simple_list_item_2,// my row layout.xml
										null, //the cursor (null for now)
										new String[] { Contacts.DISPLAY_NAME, Phone.NUMBER },// from
										new int[] { android.R.id.text1, android.R.id.text2 }, // to
										0); //flags (none)
		
		setListAdapter(mCursorAdapter);
		
		// Loader to get a list of contacts.
		//Either re-connect with an existing one, or start a new one.
		getLoaderManager().initLoader(LOADER_CONTACTS_W_NUMBERS, null, this);
	}
	
	/**
	 * Called when a new loader needs to be created. Here we create the query that will return us,
	 * asynchronously, the list of active games.
	 * 
	 * @see android.support.v4.app.LoaderManager.LoaderCallbacks#onCreateLoader(int, android.os.Bundle)
	 */
	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Uri baseUri;

        baseUri = Contacts.CONTENT_URI;
        // Now create and return a CursorLoader that will take care of
        // creating a Cursor for the data being displayed.
        String select = "((" + Contacts.DISPLAY_NAME + " NOTNULL) AND ("
//                + Contacts.HAS_PHONE_NUMBER + "=1) AND ("
                + Contacts.DISPLAY_NAME + " != '' ))";
        
        
        return new CursorLoader(getActivity(), Phone.CONTENT_URI,
        		contactsWithNumbers.PROJECTION, select, null,
                Contacts.DISPLAY_NAME + " COLLATE LOCALIZED ASC");
	}
	
    private interface contactsWithNumbers {
    	String[] PROJECTION = {
    			Contacts._ID,
    			Contacts.DISPLAY_NAME,
    			Phone.NUMBER
    	};
    	
    	int _INT = 0;
    	int DISPLAY_NAME = 1; 
    	int NUMBER = 2;
    }

	/**
	 * Here we swap in the new cursor. CLosing will be handled by the actual fragment
	 * 
	 * @see android.support.v4.app.LoaderManager.LoaderCallbacks#onLoadFinished(android.support.v4.content.Loader, java.lang.Object)
	 */
	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
		mCursorAdapter.swapCursor(data);
	}

	/**
	 * Called when the last cursor sent to OnLoadFinished is about to be closed. We just
	 * swap for a null cusor. No damage done.
	 * 
	 * @see android.support.v4.app.LoaderManager.LoaderCallbacks#onLoaderReset(android.support.v4.content.Loader)
	 */
	@Override
	public void onLoaderReset(Loader<Cursor> arg0) {
		mCursorAdapter.swapCursor(null);
	}

	/**
	 * A contact (opponent) has been selected. Tell Activity!
	 */
	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		
		
		// to keep the choice highlighted
		l.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		l.setSelection(position);
		 		
		if (mListener != null) {
			
			Cursor selectedItem = (Cursor) mCursorAdapter.getItem(position);
			mListener.onOpponentSelected( selectedItem.getString(contactsWithNumbers.DISPLAY_NAME),
						PhoneNumberUtils.stripSeparators(selectedItem.getString(contactsWithNumbers.NUMBER)));
			
		}
	}
	
	
	/**
	 * Container activity must implement this. INterface to tell
	 * activity that an opponent has been  selected
	 * 
	 * @author 	Sergio Bernales
	 * @date 	Mar 23, 2012
	 *
	 * Copyright 2012 Locomoti LLC. All rights reserved.
	 *
	 */
	public interface OnOpponentSelectedListener {
		public void onOpponentSelected(String name, String telnum);
	}

	/**
	 * Always save state.
	 */
	@Override
	public void onSaveInstanceState(Bundle outState) {
		// called when activity has stopped
		super.onSaveInstanceState(outState);
		
		Log.d(TAG, " saving the state");
	}
	
	/**
	 * Called when fragment looses focus. When
	 * the fragment is swiped 2 away.
	 */
	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		Log.d(TAG, " on stop");
		super.onStop();
	}
}
