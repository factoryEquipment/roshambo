/**
 * 
 */
package com.locomoti.android.games.roshambo.fragments;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.locomoti.android.games.roshambo.R;
import com.locomoti.android.games.roshambo.home.ui.HomeActivity;
import com.locomoti.android.games.roshambo.util.GameConstants;

/**
 * Displays characters for the user to select for when
 * either sending a challenge to a fRriend, or retaliating
 * to a challenge send by a friends
 * 
 * @author 	Sergio Bernales
 * @date 	Mar 12, 2012
 *
 * Copyright 2012 Locomoti LLC. All rights reserved.
 *
 */
public class CharacterSelectionFragment extends Fragment implements OnClickListener {
	private static final String TAG = "CharacterSelectionFragment";
	
	private CharacterSelectionInterface mListener;
	
	/**
	 * Called when fragment is attached to the Activity. First method
	 * in the fragment lifecycle that is added.
	 * 
	 */
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		
        try {
            mListener = (CharacterSelectionInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnArticleSelectedListener");
        }
	}
	
	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		// inflate our view
		View v = inflater.inflate(R.layout.fragment_character_selection, container, false);
		
		v.findViewById(R.id.cowboy).setOnClickListener(this);
		v.findViewById(R.id.bear).setOnClickListener(this);
		v.findViewById(R.id.ninja).setOnClickListener(this);
		v.findViewById(R.id.go_bubble).setOnClickListener(this);
		
		return v;
		
	}
	
	
	 /**
	 * On Click listener that handles one
	 * of the characters being selected
	 */
	@Override
	public void onClick(View v) {
		// character image id that got clicked
		int viewID = v.getId();
		
		// determine which character was clicked
		switch(viewID) {
			case R.id.cowboy:
				mListener.onCharacterSelected(GameConstants.COWBOY_MOVE);
				Log.d(TAG, "cowboy selected");
				break;
			case R.id.bear:
				mListener.onCharacterSelected(GameConstants.BEAR_MOVE);
				Log.d(TAG, "bear selected");
				break;
			case R.id.ninja:
				mListener.onCharacterSelected(GameConstants.NINJA_MOVE);
				Log.d(TAG, "ninja selected");
				break;
			case R.id.go_bubble:
				mListener.onSubmit(v);
				return;
			default:
				Log.d(TAG, "nothing selected");
				return;
		}
		
		// show the game summary
		TextView gameSummary = (TextView) getView().findViewById(R.id.game_summary);
		gameSummary.setText(mListener.getGameSummary());
		gameSummary.setVisibility(View.VISIBLE);
		gameSummary.invalidate();
		
		// show the go/submit button
		ImageView goImage = (ImageView) getView().findViewById(R.id.go_bubble);
		goImage.setVisibility(View.VISIBLE);
	}
	
	
	/**
	 * Interface parent activity should implement to receive
	 * events from the character selection fragment. Character 
	 * Selection is the last step of the move selection process ALWAYS!
	 * @author 	Sergio Bernales
	 * @date 	Apr 18, 2012
	 *
	 * Copyright 2012 Locomoti LLC. All rights reserved.
	 *
	 */
	public interface CharacterSelectionInterface {

		/**
		 * Interface parent activity should implement
		 * to receive notification that a character has been selected
		 * 
		 */
		public void onCharacterSelected(String character);
		
		
		/**
		 * The character selection is always the last task before a game
		 * is complete enough to be submitted. Activity should implmenet
		 * this interface so that activity can be notified that submission
		 * has been requested
		 */
		public void onSubmit(View v);
		
		
		/**
		 * Interface  get the fame summary from the parent activity
		 * @return
		 */
		public String getGameSummary();
	}
}
