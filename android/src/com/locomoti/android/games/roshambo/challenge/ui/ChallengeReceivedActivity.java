/**
 * 
 */
package com.locomoti.android.games.roshambo.challenge.ui;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.Window;
import android.widget.Toast;

import com.locomoti.android.games.roshambo.R;
import com.locomoti.android.games.roshambo.challenge.adapters.ChallengeReceivedPagerAdapter;
import com.locomoti.android.games.roshambo.fragments.CharacterSelectionFragment.CharacterSelectionInterface;
import com.locomoti.android.games.roshambo.home.ui.HomeActivity;
import com.locomoti.android.games.roshambo.provider.RoshamboAsyncQueryHandler;
import com.locomoti.android.games.roshambo.provider.RoshamboAsyncQueryHandler.AsyncQueryListener;
import com.locomoti.android.games.roshambo.provider.RoshamboContract.Game;
import com.locomoti.android.games.roshambo.provider.RoshamboContract.Matches;

/**
 * @author 	Sergio Bernales
 * @date 	Apr 16, 2012
 *
 * Copyright 2012 Locomoti LLC. All rights reserved.
 *
 */
public class ChallengeReceivedActivity extends FragmentActivity implements CharacterSelectionInterface,
									AsyncQueryListener{
	private static final String TAG = "ChallengeReceivedActivity";
	
	public static final String EXTRA_CHALLENGE_VALUES = "com.locomoti.android.games.roshambo.challenge.ui.challengeDataValues";
	
	RoshamboAsyncQueryHandler mAsyncQueryHandler;
	
	static final int TOKEN_CHALLENGE_RESPONSE = 0;
	
	ContentValues mChallengeReceivedValues;
	Uri mChallengeUri;

	private ChallengeReceivedPagerAdapter mFragmentAdapter;

	private ViewPager mChallengedReceivedViewPager;
	
	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		
        // Remove the title and make fulls screen
        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
//                                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        // from the extras, get our game info, it should have been created already
        Bundle extras = getIntent().getExtras();
        mChallengeReceivedValues = (ContentValues) extras.getParcelable(EXTRA_CHALLENGE_VALUES);
        
        // if values dont exists, means everything is wrong. Just start HomeActivity
        if(mChallengeReceivedValues == null){
    		startActivity(new  Intent(this, HomeActivity.class));
    		finish();
        }
        
        // setup the UI
        // set the content view to our Challenge Response View Pager
        setContentView(R.layout.challenge_response_view_pager);
        
        // get our view pager 
        mChallengedReceivedViewPager = (ViewPager) findViewById(R.id.challenge_response_view_pager);
        
        // create a new view pager adapter
        mFragmentAdapter = new ChallengeReceivedPagerAdapter(this, mChallengedReceivedViewPager, mChallengeReceivedValues);
        
        // set view pager's adapter to our custom adapter
        mChallengedReceivedViewPager.setAdapter(mFragmentAdapter);
        
        // insert the challenge into our android db
        mAsyncQueryHandler = new RoshamboAsyncQueryHandler(getContentResolver(), this);
        mAsyncQueryHandler.startInsert(TOKEN_CHALLENGE_RESPONSE, null, Game.buildChallengeReceivedURI(), mChallengeReceivedValues);
	}
	
	
	/* (non-Javadoc)
	 * @see com.locomoti.android.games.roshambo.fragments.CharacterSelectionFragment.OnCharacterSelectedListener#onCharacterSelected(java.lang.String)
	 */
	@Override
	public void onCharacterSelected(String character) {
		Toast t = Toast.makeText(this, "character selected", 3000);
		
		mChallengeReceivedValues.put(Matches.MATCH_MY_MOVE, character);
	}

	/**
	 * Submit the move in response to the received challenge
	 */
	@Override
	public void onSubmit(View v) {
		Toast t = Toast.makeText(this, "You have submitted stuff", 3000);
		
		// send insert call to content provider
//		mAsyncQUeryHandler.startInsert(TOKEN_SUBMIT_GAME, mNewGameVO, Game.buildNewGameURI(), mNewGameValues);
		
		
		// 
		mAsyncQueryHandler.startUpdate(TOKEN_CHALLENGE_RESPONSE, null, Matches.buildMatchMoveUri(mChallengeUri), mChallengeReceivedValues, null, null);
		
		// finsih because this is all this activity is
		finish();
	}

	/* (non-Javadoc)
	 * @see com.locomoti.android.games.roshambo.provider.RoshamboAsyncQueryHandler.AsyncQueryListener#onQueryComplete(int, java.lang.Object, android.database.Cursor)
	 */
	@Override
	public void onQueryComplete(int token, Object cookie, Cursor cursor) {

		
	}

	/* (non-Javadoc)
	 * @see com.locomoti.android.games.roshambo.provider.RoshamboAsyncQueryHandler.AsyncQueryListener#onInsertComplete(int, java.lang.Object, android.net.Uri)
	 */
	@Override
	public void onInsertComplete(int token, Object cookie, Uri uri) {
		
		switch(token) {
			case TOKEN_CHALLENGE_RESPONSE:
				mChallengeUri = uri;
				// challenge was inserted into android db, no we are ready to 
				// ask user for move
			default:
				break;
		}
		
	}

	/* (non-Javadoc)
	 * @see com.locomoti.android.games.roshambo.provider.RoshamboAsyncQueryHandler.AsyncQueryListener#onUpdateComplete(int, java.lang.Object, int)
	 */
	@Override
	public void onUpdateComplete(int token, Object cookie, int result) {
		// TODO Auto-generated method stub
		
	}


	/* (non-Javadoc)
	 * @see com.locomoti.android.games.roshambo.fragments.CharacterSelectionFragment.CharacterSelectionInterface#getGameSummary()
	 */
	@Override
	public String getGameSummary() {
		// TODO Auto-generated method stub
		return "ahoy";
	}
	

}
