/**
 * 
 */
package com.locomoti.android.games.roshambo.challenge.adapters;

import com.locomoti.android.games.roshambo.fragments.ActiveGamesFragment;
import com.locomoti.android.games.roshambo.fragments.CharacterSelectionFragment;
import com.locomoti.android.games.roshambo.fragments.GameInfoFragment;
import com.locomoti.android.games.roshambo.fragments.HomeFragment;
import com.locomoti.android.games.roshambo.fragments.OpponentSelectionFragment;
import com.locomoti.android.games.roshambo.fragments.StakesFragment;
import com.locomoti.android.games.roshambo.home.adapters.HomeFragmentPagerAdapter;

import android.app.Activity;
import android.content.ContentValues;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

/**
 * @author 	Sergio Bernales
 * @date 	Apr 16, 2012
 *
 * Copyright 2012 Locomoti LLC. All rights reserved.
 *
 */
public class ChallengeResponsePagerAdapter extends FragmentPagerAdapter {

	public final static int GAME_INFO_FRAG_POS = 0;
	public final static int CHAR_SEL_FRAG_POS = 1;
	
	GameInfoFragment mGameInfoFragment;
	CharacterSelectionFragment mCharSelectionFragment;
	ContentValues mGameValues;
	
	Activity mContext;
	ViewPager mViewPager;

	/**
	 * @param fm
	 */
	public ChallengeResponsePagerAdapter(Activity activity, ViewPager pager, ContentValues gameValues) {
		super(((FragmentActivity)activity).getSupportFragmentManager());
		
		mContext = activity;
		mViewPager = pager;
		mGameValues = gameValues;
	}
	
	/**
	 * Return a fragment belonging to challenge response activity
	 */
	@Override
	public Fragment getItem(int position) {
		switch(position){
		case GAME_INFO_FRAG_POS:
			if (mGameInfoFragment == null)
				mGameInfoFragment = new GameInfoFragment(mGameValues); 
			return mGameInfoFragment;
			
			
		case CHAR_SEL_FRAG_POS:
			if(mCharSelectionFragment == null)
				mCharSelectionFragment = new CharacterSelectionFragment();
			return mCharSelectionFragment;
			
		default:
			return null;
		}
	}

	/* (non-Javadoc)
	 * @see android.support.v4.view.PagerAdapter#getCount()
	 */
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return 2;
	}



}
