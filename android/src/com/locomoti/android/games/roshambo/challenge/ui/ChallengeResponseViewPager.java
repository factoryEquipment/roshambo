/**
 * 
 */
package com.locomoti.android.games.roshambo.challenge.ui;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;

/**
 * In charge of setting up the different Fragments for the response
 * to a challenge of a game
 * This includes:
 * 		- Game Summary
 * 		- Character selection
 * 
 * @author Sergio Bernales
 *
 */
public class ChallengeResponseViewPager extends ViewPager {

	Context mContext;
	
	/**
	 * Constructor to start a new HomeViewPager
	 * 
	 * @param context
	 */
	public ChallengeResponseViewPager(Context context, AttributeSet attr) {
		super(context, attr);
		
		mContext = context; 

	}
}
