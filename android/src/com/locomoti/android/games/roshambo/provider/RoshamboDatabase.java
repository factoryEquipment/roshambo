/**
 * 
 */
package com.locomoti.android.games.roshambo.provider;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

import com.locomoti.android.games.roshambo.provider.RoshamboContract.Game;
import com.locomoti.android.games.roshambo.provider.RoshamboContract.GameColumns;
import com.locomoti.android.games.roshambo.provider.RoshamboContract.MatchColumns;
import com.locomoti.android.games.roshambo.provider.RoshamboContract.UserColumns;

/**
 * Will manage our database of Games
 * 
 * @author 	Sergio Bernales
 * @date 	Mar 14, 2012
 *
 * Copyright 2012 Locomoti LLC. All rights reserved.
 *
 */
public class RoshamboDatabase extends SQLiteOpenHelper {
	private static final String TAG = "RoshamboDatabase";
	
	//TODO: create constants ME, YES, NO in integer format
	
	
	/** Database info the instantiate it */
	private static final String DATABASE_NAME = "roshambo.db";
	private static final int VER_SESSION_HASHTAG = 1;
	private static final int DATABASE_VERSION = VER_SESSION_HASHTAG;
	
	/** Tables that we will use that relate to challenges */
	interface Tables {
		String USER = "user";
		String GAME = "game";
		String MATCH = "match";
		
		String GAMES_JOIN_MATCH = " game " 
			+ "INNER JOIN  match ON match.game_ref_id=game._id ";
		
		String GAMES_JOIN_LATEST_MATCH = GAMES_JOIN_MATCH +
					"AND game.game_num_matches = match.match_num ";
	}
	
	/** Table to Table references */
    private interface References {
        String GAME_ID = "REFERENCES " + Tables.GAME + "(" + Game._ID + ")";
    }
	
	/**
	 * Constructor calling the parent constructor.
	 * 
	 * @param context
	 */
	public RoshamboDatabase(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		// TODO Auto-generated constructor stub
	}

	/* *
	 * Create the databases
	 * 
	 * @see android.database.sqlite.SQLiteOpenHelper#onCreate(android.database.sqlite.SQLiteDatabase)
	 */
	@Override
	public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + Tables.USER + " ("
                + BaseColumns._ID 				+ " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + UserColumns.USER_NODE_ID 		+ " TEXT," //TODO: Make reference to Game above
                + UserColumns.USER_C2DM_ID		+ " TEXT,"
                + UserColumns.USER_USERNAME 	+ " TEXT,"
                + UserColumns.USER_EMAIL		+ " TEXT,"
                + RESTColumns.REST_STATE		+ " SHORT  DEFAULT 0,"
                + RESTColumns.REST_RESULT		+ " SHORT,"
                + RESTColumns.UPDATED 			+ " LONG,"
                + RESTColumns.SYNCED 			+ " LONG)");
		
        db.execSQL("CREATE TABLE " + Tables.GAME + " ("
                + BaseColumns._ID 				+ " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + GameColumns.GAME_NODE_ID		+ " TEXT,"
                + GameColumns.GAME_OPP_ID 		+ " TEXT," // id from node.js
                + GameColumns.GAME_OPP_USERNAME	+ " TEXT,"
                + GameColumns.GAME_STAKES 		+ " TEXT NOT NULL,"
                + GameColumns.GAME_CREATOR_ME 	+ " SHORT NOT NULL,"
                + GameColumns.GAME_MY_MOVE 		+ " SHORT NOT NULL,"
                + GameColumns.GAME_STATE 		+ " SHORT NOT NULL,"
                + GameColumns.GAME_NUM_MATCHES 	+ " INTEGER NOT NULL,"
                + GameColumns.GAME_CREATED 		+ " LONG NOT NULL,"
                + GameColumns.GAME_OPP_NAME 	+ " TEXT,"
                + GameColumns.GAME_OPP_TELNUM 	+ " TEXT,"
                + GameColumns.GAME_OPP_EMAIL 	+ " TEXT,"
                + RESTColumns.REST_STATE		+ " SHORT DEFAULT 0,"
                + RESTColumns.REST_RESULT		+ " SHORT,"
                + RESTColumns.UPDATED 			+ " LONG,"
                + RESTColumns.SYNCED 			+ " LONG,"
                + "UNIQUE (" + GameColumns.GAME_NODE_ID + ") ON CONFLICT REPLACE)");
        
        db.execSQL("CREATE TABLE " + Tables.MATCH + " ("
                + BaseColumns._ID 			+ " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + MatchColumns.GAME_REF_ID 		+ " INTEGER " + References.GAME_ID + ","
                + MatchColumns.MATCH_NODE_ID 	+ " TEXT ,"
                + MatchColumns.MATCH_NUM 		+ " INTEGER ,"
                + MatchColumns.MATCH_MY_MOVE	+ " SHORT DEFAULT -1,"
                + MatchColumns.MATCH_OPP_MOVE	+ " SHORT DEFAULT -1,"
                + "UNIQUE (" + MatchColumns.MATCH_NODE_ID + ") ON CONFLICT REPLACE)");
	}

	/* (non-Javadoc)
	 * @see android.database.sqlite.SQLiteOpenHelper#onUpgrade(android.database.sqlite.SQLiteDatabase, int, int)
	 */
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
//		db.execSQL("drop table " + Tables.USER);
//		db.execSQL("drop table " + Tables.GAME);
//		db.execSQL("drop table " + Tables.MATCH);

	}
}
