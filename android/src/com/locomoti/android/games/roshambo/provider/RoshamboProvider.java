/**
 * 
 */
package com.locomoti.android.games.roshambo.provider;

import java.util.Arrays;
import java.util.Map;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.provider.BaseColumns;
import android.util.Log;


import com.locomoti.android.games.roshambo.home.services.ServiceHelper;
import com.locomoti.android.games.roshambo.provider.RoshamboContract.Game;
import com.locomoti.android.games.roshambo.provider.RoshamboContract.Matches;
import com.locomoti.android.games.roshambo.provider.RoshamboContract.QualifiedColumns;
import com.locomoti.android.games.roshambo.provider.RoshamboContract.User;
import com.locomoti.android.games.roshambo.provider.RoshamboDatabase.Tables;
import com.locomoti.android.games.roshambo.util.GameConstants;
import com.locomoti.android.games.roshambo.vo.JsonVO;
import com.locomoti.android.games.roshambo.vo.MatchVO;
import com.locomoti.android.games.roshambo.vo.MoveVO;
import com.locomoti.android.games.roshambo.vo.NewGameVO;
import com.locomoti.android.games.roshambo.vo.UserVO;

/**
 * Implements the access to our database. The CRUD methods.
 * This method is also responsible for calling our Rest service
 * to send these synchronize these CRUD calls back to the server.
 * 
 * @author 	Sergio Bernales
 * @date 	Mar 14, 2012
 *
 * Copyright 2012 Locomoti LLC. All rights reserved.
 *
 */
public class RoshamboProvider extends ContentProvider {
	private static final String TAG = "RoshamboProvider";
	
	/** Reference to our database */
	private RoshamboDatabase mOpenHelper;
	
	/** Reference to our Service Helper(intent creator for our IntentService */
	private ServiceHelper mServiceHelper;
	
	
	/** Our URI types*/
	private static final int USER = 100; // query, insert
	private static final int USER_RETRY = 101; // update (retry user creation)
	
	private static final int NEW_GAME = 200; // insert game, query game
	private static final int GAMES_ACTIVE = 201; // for query only
	private static final int GAME = 202; // insert game, query game
	
	private static final int CHALLENGE_RECEIVED = 203;
	
	private static final int MATCH_MOVE = 300;
	private static final int MATCH_MOVE_RESPONSE = 301;
	
	/** shared preference file */
	private static final String PROVIDER_PREFERENCES = "prefs";
	
	/**
	 * Build our URImatcher that catches
	 * all URI variations supported by this content provider.
	 * Basically we map paths to a type numbers so that in
	 * getType URI we can do a switch/case to return a type instead
	 * of if/else's.
	 */
	private static final UriMatcher mUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
	static {
		final String authority = RoshamboContract.CONTENT_AUTHORITY;
		
		mUriMatcher.addURI(authority, "user", USER);
		mUriMatcher.addURI(authority, "user/*", USER);
		mUriMatcher.addURI(authority, "games/active", GAMES_ACTIVE);
		mUriMatcher.addURI(authority, "games/new", NEW_GAME);
		mUriMatcher.addURI(authority, "games/#", GAME);
		mUriMatcher.addURI(authority, "games/challenge", CHALLENGE_RECEIVED);
		mUriMatcher.addURI(authority, "games/#/match/move", MATCH_MOVE);
		mUriMatcher.addURI(authority, "games/match/move/response", MATCH_MOVE_RESPONSE);
	}
	
	/* (non-Javadoc)
	 * @see android.content.ContentProvider#getType(android.net.Uri)
	 */
	@Override
	public String getType(Uri uri) {
		final int match = mUriMatcher.match(uri);
		switch(match) {
			case USER:
				return User.CONTENT_TYPE;
			case GAMES_ACTIVE:
				return Game.CONTENT_TYPE;
			default:
				throw new UnsupportedOperationException("Unknown uri: " + uri);
		}
		
	}
	
	
	/* (non-Javadoc)
	 * @see android.content.ContentProvider#onCreate()
	 */
	@Override
	public boolean onCreate() {
		final Context context = getContext();
		
		mServiceHelper = new ServiceHelper();
		
		mOpenHelper = new RoshamboDatabase(context);
		if(mOpenHelper == null)
			return false;
		
		return true;
	}
	
	/* (non-Javadoc)
	 * @see android.content.ContentProvider#delete(android.net.Uri, java.lang.String, java.lang.String[])
	 */
	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		// TODO Auto-generated method stub
		return 0;
	}



	/* (non-Javadoc)
	 * @see android.content.ContentProvider#insert(android.net.Uri, android.content.ContentValues)
	 */
	@Override
	public Uri insert(Uri uri, ContentValues values) {
//		Log.d(TAG, "insert(uri=" + uri + ", values=" + values.toString() + ")");
		final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
		final int match = mUriMatcher.match(uri);
		
		switch (match){
			case USER: {
				
				// Insert a new user record into db (conflict_replace will replace a record
				// if it already exists if say our user creation process was interrupted
				long userID = db.insertWithOnConflict(Tables.USER, null, values, SQLiteDatabase.CONFLICT_REPLACE);	
				if (userID == -1)
					return null;
				
				// send user creation request to our intent service
				mServiceHelper.createUser(getContext(), values);
				
				// default URI to return, remember this wasnt created, it will be when server gives us a user ID
				return User.buildUserUri(User.DEFAULT_ID);
			}
			
			case NEW_GAME: {
				Uri newGameUri = insertNewGame(values, db);
				return newGameUri;
			}
			
			case CHALLENGE_RECEIVED: {
				return insertNewChallengeReceived(values, db);
			}
				
				
			
			default: {
				throw new UnsupportedOperationException("Unknown uri: " + uri);
			}
		}
	}

	/**
	 * Insert a new game from the challenge data received.
	 * Specifically a new game and match
	 * @param newChallengeReceivedalues
	 * @param db
	 * @return 
	 */
	private Uri insertNewChallengeReceived(ContentValues newChallengeReceivedalues,
			SQLiteDatabase db) {
		
		long gameID = -1;
		
		/// **** BEGIN TRANSACTION ****
		db.beginTransaction();

		try {
			
			// get current time to give all records a common timestamp
			long timestamp = GameConstants.getTimestamp();
			
			// temp contentvalue object that will hold values for respective inserts
			ContentValues tempValueHolder = new ContentValues();
			
			// create new Game table record holding our new data
			tempValueHolder.putAll(newChallengeReceivedalues);// we need all the passed in valued
	
			tempValueHolder.put(Game.GAME_CREATOR_ME, GameConstants.SHORT_FALSE);
			tempValueHolder.put(Game.GAME_MY_MOVE, GameConstants.SHORT_TRUE);
			tempValueHolder.put(Game.GAME_STATE, GameConstants.STATE_ACTIVE);
			tempValueHolder.put(Game.GAME_NUM_MATCHES, 1);// this is the first match
			
	//		tempValueHolder.put(Game.GAME_OPP_NAME, "temp: remove this field");//TODO: this is the first match
			
			// insert new game
			gameID = db.insert(Tables.GAME, null, tempValueHolder);
			if (gameID == -1)
				return null;
	
			
			// now create the new match
			tempValueHolder.clear();
			
			tempValueHolder.put(Matches.GAME_REF_ID, gameID);
			tempValueHolder.put(Matches.MATCH_NUM, 1);
			
			// insert new match
			long matchID = db.insert(Tables.MATCH, null, tempValueHolder);				
			if (matchID == -1)
				return null;
			
			db.setTransactionSuccessful();
		} finally {
		/// **** END TRANSACTION ****
			db.endTransaction();

		}
		
		return Game.buildGameURI(gameID);
	}


	/**
	 * Insert the required records for a newchallenge that the user is submitting
	 * A game and a match specifically
	 * 
	 * @param newGameValues 
	 * @param db 
	 * 
	 */
	private Uri insertNewGame(ContentValues newGameValues, SQLiteDatabase db) {
		
		long gameID = -1;
		
		/// **** BEGIN TRANSACTION ****
		db.beginTransaction();

		try {
			/** 
			 * creating a new challenge. We need to do 3 things:
			 * 1. insert record in game, 2. insert record in match,
			 * 3. send new game to server 
			 */
			// get current time to give all records a common timestamp
			long timestamp = GameConstants.getTimestamp();
			
			// temp contentvalue object that will hold values for respective inserts
			ContentValues tempValueHolder = new ContentValues();
			
			// create new Game table record holding our new data
			tempValueHolder.putAll(newGameValues);// we need all the passed in valued
			tempValueHolder.remove(Matches.MATCH_MY_MOVE); //except my move:not a field in game table
			tempValueHolder.put(Game.GAME_STAKES, newGameValues.getAsString(Game.GAME_STAKES));
			tempValueHolder.put(Game.GAME_CREATOR_ME, GameConstants.SHORT_TRUE);
			tempValueHolder.put(Game.GAME_MY_MOVE, GameConstants.SHORT_FALSE);
			tempValueHolder.put(Game.GAME_STATE, GameConstants.STATE_ACTIVE);
			tempValueHolder.put(Game.GAME_NUM_MATCHES, 1);// this is the first match
			tempValueHolder.put(Game.GAME_CREATED, timestamp);
			tempValueHolder.put(Game.REST_STATE, GameConstants.REST_POSTING);
			
			// insert new game
			gameID = db.insert(Tables.GAME, null, tempValueHolder);
			if (gameID == -1)
				return null;
			
			// clear value holder to reuse for inserting new match data
			tempValueHolder.clear();
			
			tempValueHolder.put(Matches.GAME_REF_ID, gameID);
			tempValueHolder.put(Matches.MATCH_MY_MOVE, newGameValues.getAsShort(Matches.MATCH_MY_MOVE));
			tempValueHolder.put(Matches.MATCH_NUM, 1);
			
			// insert new match
			long matchID = db.insert(Tables.MATCH, null, tempValueHolder);				
			if (matchID == -1)
				return null;
			
			Log.d(TAG, "Inserting from the content provider");
			db.setTransactionSuccessful();
		} finally {
		/// **** END TRANSACTION ****
			db.endTransaction();

		}
		
		/** 
		 * Send game to service helper so it can send request to our rest service
		 * */
		// add android db record id to give it context in the http response
		newGameValues.put(NewGameVO.ANDROID_ID_FIELD, Long.toString(gameID));
		
		// send it off!
		mServiceHelper.createNewGame(getContext(), newGameValues);
		
		return Game.buildGameURI(gameID);		
	}


	/* (non-Javadoc)
	 * @see android.content.ContentProvider#query(android.net.Uri, java.lang.String[], java.lang.String, java.lang.String[], java.lang.String)
	 */
	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		Log.d(TAG, "query(uri=" + uri + ", proj=" + Arrays.toString(projection) + ")");
		final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
		
		final int match = mUriMatcher.match(uri);
		switch (match){
			case USER: {
				Cursor c = null;
				try{
					c = db.query(Tables.USER, projection, selection, null, null, null, null);
				}
				catch(SQLException e ){
					e.getMessage();
					e.printStackTrace();
				}
				return c;
			}
			case GAMES_ACTIVE: {
								//build args, should only by state ACTIVE
				//TODO: the order by NULL should be sorted by game created by me or not
				String[] args = {Short.toString(GameConstants.STATE_ACTIVE)};
				
				//launch the queryx
				return db.query(Tables.GAMES_JOIN_MATCH, projection,Game.LATEST_MATCH_ACTIVE_GAME_SELECTION,
						args, null, null, null);
			}
			case GAME: {
				// get the game id we are querying for
				String gameID = uri.getLastPathSegment();
				
				// set it as an argument
				String[] args = {gameID};
				
				//launch the query
				return db.query(Tables.GAME, projection, Game.WHERE_SPECIFIC_GAME, args, null, null, null);
			}
			default: {
				throw new UnsupportedOperationException("Unknown uri: " + uri);
			}
		}
	}

	/* (non-Javadoc)
	 * @see android.content.ContentProvider#update(android.net.Uri, android.content.ContentValues, java.lang.String, java.lang.String[])
	 */
	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		Log.d(TAG, "update(uri=" + uri + ", values=" + values.toString() + ")");
		final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
		
		final int match = mUriMatcher.match(uri);
		switch (match){
			case USER: {
				// add values to user prefereces that we will constant reuse for every subsequent call
				SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());				
				Editor editor = prefs.edit();
				editor.putString(User.USER_USERNAME, (String) values.get(User.USER_USERNAME));
				editor.putString(User.USER_NODE_ID, (String) values.get(User.USER_NODE_ID));
				editor.commit();

				// update our user record
				int r = db.update(Tables.USER, values, selection, null);
				getContext().getContentResolver().notifyChange(uri, null);
				
				return r;
			}
			
			case NEW_GAME: {
				Log.d(TAG, "hellow world");
				int r = db.update(Tables.GAME, values, selection, selectionArgs);

				getContext().getContentResolver().notifyChange(Game.buildGameRequestProcessedURI(), null);
				getContext().getContentResolver().notifyChange(Game.buildActiveGamesURI(), null);
				
				return r;
			}
			
			case MATCH_MOVE: {
				// get the game id from the uri
				String gameID = uri.getPathSegments().get(1);
				
				return submitMatchMove(gameID, values, db);
			}		
			case MATCH_MOVE_RESPONSE: {
				
				// get the game id from the value object
				String gameID = values.getAsString(Game.GAME_NODE_ID);
				
				return setGameRestStatus(gameID, GameConstants.REST_NEUTRAL, GameConstants.HTTP_SUCCESS, db);
			}
			default:{
				return -1;
			}
		}
	}


	/**
	 * Updates our game tables with the new user submitted move
	 * and send it off to the rest service
	 * @param gameID 
	 * @param values 
	 * @param db 
	 * 
	 * @return
	 */
	private int submitMatchMove(String gameID, ContentValues values, SQLiteDatabase db) {
		int recsUpdated = -1;
		
		/// **** BEGIN TRANSACTION ****
		db.beginTransaction();

		try {
		
			// check if the game has a latest match we havent submitted a move for
			if (!isGamePlayable(db, gameID))
				return -1;
			
			// set our records status to PUTTING
			recsUpdated = setGameRestStatus(gameID, GameConstants.REST_PUTTING, GameConstants.HTTP_NULL, db);
			
			// we only should have updated one and only one
			if (recsUpdated != 1)
				return -1;
			
			// create new value object for just the move the user submitted
			ContentValues moveValue = new ContentValues(1);
			moveValue.put(Matches.MATCH_MY_MOVE, values.getAsString(Matches.MATCH_MY_MOVE));
			
			// updated our match record with the move
			recsUpdated = db.update(Tables.MATCH, moveValue, Matches.WHERE_GAME_REF_AND_UNPLAYED_MOVE,
					new String[]{gameID});
			
			if(recsUpdated != 1)
				return -1;

			db.setTransactionSuccessful();
		} finally {
		/// **** END TRANSACTION ****
			db.endTransaction();
		}
			
		// add the user id to the move values
		values.put(User.USER_NODE_ID, GameConstants.getUserNodeID(getContext()));
		
		// send off to the cloud
		mServiceHelper.submitMatchMove(getContext(), values);
			
		return recsUpdated;
	}


	/**
	 * Set the table's rest status columns to a specific status.
	 * 
	 * @param table
	 * @param restStatus
	 * @param httpResult 
	 * @param db 
	 */
	private int setGameRestStatus(String gameID, short restStatus, short httpResult, SQLiteDatabase db) {
		ContentValues values = new ContentValues(1);
		values.put(Game.REST_STATE, restStatus);
		
		// if we were given an http results as well, put 
		if (httpResult != GameConstants.HTTP_NULL){
			values.put(Game.REST_RESULT, httpResult);
		}
			
		
		return db.update(Tables.GAME, values, Game.WHERE_SPECIFIC_GAME, new String[]{gameID});
		
	}


	/**
	 * Check to see that 
	 *
	 * @param uri
	 * @param db 
	 * @return
	 */
	private boolean isGamePlayable(SQLiteDatabase db, String gameID) {
		// get the latest match in the game
		Cursor gameCursor =  db.query(Tables.GAMES_JOIN_MATCH, GamePlayableQuery.PROJECTION, GamePlayableQuery.SELECTION,
				new String[] {Short.toString(GameConstants.STATE_ACTIVE), gameID}, null, null, null);

		
		if (gameCursor == null || gameCursor.getCount() != 1)
			return false;
		
		gameCursor.moveToFirst();
		
		Short restState = gameCursor.getShort(GamePlayableQuery._ID);
		
		return true;
	}
	
	
	/**
	 * DB query constants to get the latest game 
	 */
	private interface GamePlayableQuery {
		String[] PROJECTION = {
			QualifiedColumns.GAME__ID
        };
        
        String SELECTION = Game.LATEST_MATCH_ACTIVE_GAME_SELECTION + " AND "
						+  Game.WHERE_SPECIFIC_GAME + " AND "
        				+  Game.GAME_MY_MOVE+"="+GameConstants.SHORT_TRUE+" AND " 
        				+  Matches.MATCH_MY_MOVE+"=-1 AND "// I didnt perform a move yet
        				+  Game.REST_STATE+"="+GameConstants.REST_NEUTRAL; //request has not been sent already

        int _ID = 0;
	}
	

	
}
