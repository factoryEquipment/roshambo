/**
 * 
 */
package com.locomoti.android.games.roshambo.provider;

import android.net.Uri;
import android.provider.BaseColumns;

import com.locomoti.android.games.roshambo.provider.RoshamboDatabase.Tables;

/**
 * Following iosched (Google IO 2010) best 
 * practices (ScheduleContract.java)
 * 
 * Here we define Games table columnd and ways to interact
 * with them.
 * 
 * @author 	Sergio Bernales
 * @date 	Mar 14, 2012
 *
 * Copyright 2012 Locomoti LLC. All rights reserved.
 *
 */
public class RoshamboContract {
	
	interface UserColumns {
		/** user UID coming from the server (what we send to node)*/
		String USER_NODE_ID = "user_node_id";
		/** Name of the user */
		String USER_USERNAME = "user_username";
		/** Email of the user */
		String USER_EMAIL = "user_email";
		/** C2DM registration ID used to receive push notifications */
		String USER_C2DM_ID = "user_c2dm_id";
	}
	
	/**
	 * Columns that represent a game. A challenge if I may.
	 *  
	 */
	interface GameColumns {
		/** ID coming from the server. This is very unique.*/
		String GAME_NODE_ID = "game_node_id";
		/** ID of opponent*/
		String GAME_OPP_ID = "game_opp_node_id";
		/** ID of opponent*/
		String GAME_OPP_USERNAME = "game_opp_username";
		/** The stakes  of the challenge*/
		String GAME_STAKES = "game_stakes";
		/** Whether I initiated the challenge or not*/
		String GAME_CREATOR_ME = "game_creator_me";
		/** Whether I need to submit my move or not*/
		String GAME_MY_MOVE = "game_my_move";
		/** Whether game is active, I won, or I lost (game states above)*/
		String GAME_STATE = "game_state";
		/** Number of matches played (inclusive of the one being played) */
		String GAME_NUM_MATCHES = "game_num_matches";
		/** Time the game was created*/
		String GAME_CREATED = "game_created";
		/** Name of the opponent */
		String GAME_OPP_NAME = "opp_name";
		/** Phone number of the opponent */
		String GAME_OPP_TELNUM = "opp_telnum";
		/** Phone number of the opponent */
		String GAME_OPP_EMAIL = "opp_email";
		/** Results Pending*/
		String GAME_RESULTS_PENDING = "results_pending";
	}
	
	/**
	 * All the matches that correspond to a given game.
	 * There can be many of these for a game, but there has to be at least one
	 * 
	 */
	interface MatchColumns {
		/** ID of the game this match belongs to. This is the Android SQLite ID*/
		String GAME_REF_ID= "game_ref_id";
		/** ID for this match coming from the server*/
		String MATCH_NODE_ID = "match_node_id";
		/** My move for this match*/
		String MATCH_MY_MOVE = "match_my_move";
		/** Opponent's move for this match*/
		String MATCH_OPP_MOVE= "match_opponent_move";
		/** The match number in the context of the game" */
		String MATCH_NUM = "match_num";
	}
	
	/**
	 * The results of a match. A record in here means a particular game has results
	 * the user has not seen yet. It could be a tie win or loss.
	 *
	 */
	interface PendingResultColumns {
		/** ID coming from the server. This is very unique. */
		String GAME_NODE_ID = "game_node_id";
		/** TOKEN received from server as to what the result was */
		String RESULT_C2DM_TOKEN = "result_c2dm_token";
		/** Strip to show*/
		String RESULT_COMIC_STRIP = "result_strip";
	}
	
	/** Items with which to build a URI */
	public static final String CONTENT_AUTHORITY = "com.locomoti.android.games.roshambo.provider.roshambo";
	private static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);
	
	
	/** Path for our different tables */
	private static final String PATH_USER = "user";
	private static final String PATH_GAME = "games";
	private static final String PATH_MATCH = "match";
	private static final String PATH_ACTIVE = "active";
	private static final String PATH_NEW = "new";
	private static final String PATH_USER_EXISTS = "exists";
	private static final String PATH_GAME_REQ_PROCESSED = "processed";
	private static final String PATH_CHALLENGE_RECEIVED = "challenge";
	private static final String PATH_MOVE = "move";
	private static final String PATH_RESPONSE = "response";
	
	/**
	 * Our user data. We usually are mostly interest in the GAME_NAME and GAME_ID
	 * as these are the ones we pass to the server
	 * 
	 *
	 */
	public static class User implements UserColumns, RESTColumns, BaseColumns{
		public static final Uri CONTENT_URI =
            BASE_CONTENT_URI.buildUpon().appendPath(PATH_USER).build();
		
		public static final String CONTENT_TYPE = 
			"vnd.android.cursor.item/vnd.roshambo.user";
		
		// there will always be only one user
		public static final int DEFAULT_ID = 1;
		
		public static final String DEFAULT_WHERE = "_id = 1";
        
        /**
         * Uri with the official Android User ID appended :)
         */
        public static Uri buildUserUri(int userID) {
        	return CONTENT_URI.buildUpon().appendPath(Integer.toBinaryString(userID)).build();
        }
        
        /**
         * Uri to query for opponent existance/id
         */
        public static Uri buildUserExistsUri() {
        	return CONTENT_URI.buildUpon().appendPath(PATH_USER_EXISTS).build();
        }
	}
		
	
	/**
	 * Each Game is  one that either the user has started
	 * or the user has been challenged by someone else 
	 *
	 */
	public static class Game implements GameColumns, RESTColumns, BaseColumns{
		public static final Uri CONTENT_URI =
            BASE_CONTENT_URI.buildUpon().appendPath(PATH_GAME).build();
		
		/** Default "ORDER BY" clause. */
        public static final String DEFAULT_SORT = GameColumns.GAME_CREATED + " DESC";

		public static final String CONTENT_TYPE = 
				"vnd.android.cursor.dir/vnd.roshambo.game";
		
		public static final String LATEST_MATCH_ACTIVE_GAME_SELECTION = 
				" game_num_matches=match_num AND game_state=? ";
		
		public static final String WHERE_SPECIFIC_GAME = "game._id=?";
		
		/**
         * Build URI that deals with only NEW games
         * 
         */
        public static Uri buildNewGameURI() {
            return CONTENT_URI.buildUpon().appendPath(PATH_NEW).build();
        }

		/**
         * Build URI that pulls only active games for the current user.
         * 
         */
        public static Uri buildActiveGamesURI() {
            return CONTENT_URI.buildUpon().appendPath(PATH_ACTIVE).build();
        }
        
        /**
         * Build URI that pulls only active games for the current user.
         * 
         */
        public static Uri buildGameURI(long gameID) {
            return CONTENT_URI.buildUpon().appendPath(Long.toString(gameID)).build();
        }
        
        /**
         * Build URI that notifies game request has returned from the server 
         * (used mostly for notifying the UI contentobserver that request has been process
         * so we can turn off the "loading" animation.
         */
        public static Uri buildGameRequestProcessedURI() {
            return CONTENT_URI.buildUpon().appendPath(PATH_GAME_REQ_PROCESSED).build();
        }

		/**
		 * @return
		 */
		public static Uri buildChallengeReceivedURI() {
			// TODO Auto-generated method stub
			return CONTENT_URI.buildUpon().appendPath(PATH_CHALLENGE_RECEIVED).build();
		}
	}
	
	/**
	 * each match has a parent. Multiple matches can have a parent
	 * which means there has been several ties in tha match 
	 *
	 */
	public static class Matches implements MatchColumns, BaseColumns{
		public static final Uri CONTENT_URI =
            BASE_CONTENT_URI.buildUpon().appendPath(PATH_MATCH).build();
		
		public static final String WHERE_GAME_REF = 
			" game_ref_id=? ";
		
		public static final String WHERE_GAME_REF_AND_UNPLAYED_MOVE = WHERE_GAME_REF 
			+ " AND match_my_move=-1 ";
		
		/**
		 * Build uri to work with mathc moves
		 * @param mChallengeUri 
		 * @param gameID
		 * @return
		 */
		public static Uri buildMatchMoveUri(Uri challengeUri) {
			return challengeUri.buildUpon().appendPath(PATH_MATCH).appendPath(PATH_MOVE).build();
		}

		/**
		 * Build URI of the repsonse the user just submitted
		 * 
		 * @return
		 */
		public static Uri buildMatchMoveResponse() {
			return Game.CONTENT_URI.buildUpon().appendPath(PATH_MATCH)
							.appendPath(PATH_MOVE).appendPath(PATH_RESPONSE).build();
		}
	}
	
	/**
	 * Columns that are fully qualified with a specific parent
	 */
	public interface QualifiedColumns {
		public static final String GAME_NODE_ID = Tables.GAME + "." + Game.GAME_NODE_ID;
//							+ " AS " + Games.GAME_ID;
		public static final String GAME__ID = Tables.GAME + "." + Game._ID;
		public static final String MATCH__ID = Tables.MATCH + "." + Matches._ID;
//							+ " AS " + Games._ID;
		
	}
}
