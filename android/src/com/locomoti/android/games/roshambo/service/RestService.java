/**
 * 
 */
package com.locomoti.android.games.roshambo.service;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;

import android.app.IntentService;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.google.android.c2dm.C2DMessaging;
import com.locomoti.android.games.roshambo.network.ResponseProcessor;
import com.locomoti.android.games.roshambo.network.RestMethodInvoker;
import com.locomoti.android.games.roshambo.provider.RoshamboContract.Game;
import com.locomoti.android.games.roshambo.provider.RoshamboContract.Matches;
import com.locomoti.android.games.roshambo.provider.RoshamboContract.User;
import com.locomoti.android.games.roshambo.util.GameConstants;
import com.locomoti.android.games.roshambo.vo.NewGameVO;
import com.locomoti.android.games.roshambo.vo.UserVO;

/**
 * Takes care of receicing rest requests from the app and
 * then delegating them to the REST Method creator.
 * It basically is a service interface to our server.
 * 
 * 
 * @author 	Sergio Bernales
 * @date 	Mar 19, 2012
 *
 * Copyright 2012 Locomoti LLC. All rights reserved.
 *
 */
public class RestService extends IntentService {
	private static final String TAG = "RestService";
	
	/** Different extras the Service Helper can send to us */
	public static final String EXTRA_NEW_USER = "extra_new_user";
	public static final String EXTRA_NEW_GAME = "extra_new_game";
	
	/** Different actions we will handle*/
	public static final String SERVICE_ACTION = "service_action";
	
	/** The API of our intent service. The actions we want completed */
	public static final int ACTION_CREATE_USER = 1;
	public static final int ACTION_CREATE_NEW_GAME = 2;
	


	
	/** stores a millisecond time value used to producing random-ish resource id values*/
	private static long MILLISECOND_DIVIDER;
	
	/** Our Jackson Json Mapper */
	//Best practices suggests to have only one Mapper
	ObjectMapper mJsonMapper;
	
	/** Our response processor */
	ResponseProcessor mProcessor;
	
	/**
	 * @param name
	 */
	public RestService() {
		super(TAG);
	}
	
	/* (non-Javadoc)
	 * @see android.app.IntentService#onCreate()
	 */
	@Override
	public void onCreate() {
		super.onCreate();
		
		mJsonMapper = new ObjectMapper();
		mProcessor = new ResponseProcessor(getContentResolver());
	}
	
	/* (non-Javadoc)
	 * @see android.app.IntentService#onHandleIntent(android.content.Intent)
	 */
	@Override
	protected void onHandleIntent(Intent intent) {
		MILLISECOND_DIVIDER = GameConstants.getTimestamp();
		
		int match = intent.getIntExtra(SERVICE_ACTION, -1);
		
		switch(match) {
			case ACTION_CREATE_USER:{
				Log.d(TAG, "We have set a create user rest request");
				
				// get the values of the new user
				ContentValues newUserValues = (ContentValues) intent.getExtras().get(EXTRA_NEW_USER);
				
				if(newUserValues == null)
					return;
			
				createUser(newUserValues);
				return;
			}
			case ACTION_CREATE_NEW_GAME: {
				Log.d(TAG, "send new game to the server");
				
				// get the values of the new game
				ContentValues newGameValues = (ContentValues) intent.getExtras().get(EXTRA_NEW_GAME);
				
				if(newGameValues == null)
					return;
				
				createNewGame(newGameValues);
				return;
			}
			default:
				return;
		}

	}

	/**
	 * Creates json version of the new game and send it off
	 * to the server. It then sends the response to the 
	 * response processor
	 * 
	 * @param newGameValues
	 */
	private void createNewGame(ContentValues newGameValues) {
		// create the new gameVO to get a json representation
		NewGameVO newGameVO = new NewGameVO();
		
		// send user id in json so node can process faster TODO: necessary?
		String userId = GameConstants.getUserNodeID(this);
		newGameVO.setField(NewGameVO.CHA_ID_FIELD, userId);
		
		// put in the android db record id so http response has contxt
		newGameVO.setField(NewGameVO.ANDROID_ID_FIELD, newGameValues.getAsString(NewGameVO.ANDROID_ID_FIELD));
		
		// put the challenger move
		newGameVO.setField(NewGameVO.CHA_MOVE, newGameValues.getAsString(Matches.MATCH_MY_MOVE));
		
		// put the opponenet info
		newGameVO.setField(NewGameVO.OPP_NAME_FIELD, newGameValues.getAsString(Game.GAME_OPP_NAME));
		newGameVO.setField(NewGameVO.OPP_TELNUM_FIELD, newGameValues.getAsString(Game.GAME_OPP_TELNUM));
		//TODO: the email field
		
		// put in the stakes
		newGameVO.setField(NewGameVO.STAKES_FIELD, newGameValues.getAsString(Game.GAME_STAKES));
		
		// build invoker
		GameConstants.createRandomResourceID(MILLISECOND_DIVIDER);
		RestMethodInvoker invoker = RestMethodInvoker.buildRestMethodInvoker(this, Resources.USER_RESOURCE, 
				GameConstants.createRandomResourceID(MILLISECOND_DIVIDER), Resources.GAME_RESOURCE);
		
		// send request
		JsonNode newGameNode = invoker.post(mJsonMapper, newGameVO);
		
		// send to process to update our db
		mProcessor.processNewGameResponse(newGameNode);
	}

	/**
	 * User the user values passed, register user
	 * to the server using a Restinvoker
	 * 
	 * @param newUserValues
	 */
	private void createUser(ContentValues newUserValues) {
		// create newUserVO
		UserVO newUser = new UserVO();
		
		// set email, username, c2dm registration id
		newUser.setField(UserVO.EMAIL_FIELD, newUserValues.getAsString(User.USER_EMAIL));
		newUser.setField(UserVO.USERNAME_FIELD, newUserValues.getAsString(User.USER_USERNAME));
		newUser.setField(UserVO.C2DM_ID_FIELD, C2DMessaging.getRegistrationId(this));
		
		// get imei and phone num and add them 
		TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
		newUser.setField(UserVO.IMEI_FIELD, tm.getDeviceId());
		newUser.setField(UserVO.TELNUM_FIELD, tm.getLine1Number());
		
		// build invoker
		RestMethodInvoker invoker = RestMethodInvoker.buildRestMethodInvoker(this, Resources.USER_RESOURCE);
		
		// execute method and get the response
		JsonNode newUserNode = invoker.post(mJsonMapper, newUser);
		
		// send over a UserVO version of our json response to the processor
		mProcessor.processNewUserResponse( newUserNode );

		//		JsonNode jsonResponse = invoker.get(mJsonMapper);

	}

	interface Resources {
		public static final String USER_RESOURCE = "/users";
		public static final String GAME_RESOURCE = "/games";
	}

}
