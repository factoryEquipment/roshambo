/**
 * 
 */
package com.locomoti.android.games.roshambo.service;

import android.app.IntentService;
import android.content.Intent;

/**
 * Processes long running backround items for our game. Many exist
 * To name a few: 
 * 	-update all current active games
 * 	-send a new challenge
 * 	-respond to a challenge
 * 	-update user account
 * 	-delete an existing game
 * 
 * @author 	Sergio Bernales
 * @date 	Mar 14, 2012
 *
 * Copyright 2012 Locomoti LLC. All rights reserved.
 *
 */
public class HomeIntentService extends IntentService {
	 private static final String TAG = "HomeIntentService";
	
	/**
	 * Constructor calling the parent constructor
	 * to name our intent service.
	 * 
	 * @param name
	 */
	public HomeIntentService() {
		super(TAG);
		// TODO Auto-generated constructor stub
	}

	/**
	 * This is where we get the intent that was send by the caller.
	 * In our case the ServiceHelper. Here we parse the intent
	 * and delegate it to the appropriate function to porcess.
	 *  
	 * @see android.app.IntentService#onHandleIntent(android.content.Intent)
	 */
	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub

	}

}
