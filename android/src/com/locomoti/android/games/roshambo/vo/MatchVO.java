/**
 * 
 */
package com.locomoti.android.games.roshambo.vo;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;

/**
 * Stores info about a match of a game. 
 * 
 * @author 	Sergio Bernales
 * @date 	Mar 24, 2012
 *
 * Copyright 2012 Locomoti LLC. All rights reserved.
 *
 */
public class MatchVO extends JsonVO {

	/**
	 * Fields of a game. These are fields that are agreed upon
	 * between the server and the device. How the server and device 
	 * actually save them is up to them. 
	 * */
	public static final String MATCH_FIELD = "match";
	
	public static final String MATCH_ID_FIELD = "matchID";
	public static final String MATCH_NUM_FIELD = "matchNum";
	public static final String CHA_MOVE_FIELD = "chaMove";
	public static final String OPP_MOVE_FIELD = "oppMove";
	
	/** 
	 * @param matchNum
	 * @param chaMove
	 * @param oppMove
	 */
	public MatchVO(int matchNum, int chaMove, int oppMove){
		super();
		
		mJsonFields.put(MATCH_NUM_FIELD, Integer.toString(matchNum));
		mJsonFields.put(CHA_MOVE_FIELD, Integer.toString(chaMove));
		mJsonFields.put(OPP_MOVE_FIELD, Integer.toString(oppMove));
	}
	
	/**
	 * Create empty match (niether challenger or opponent
	 * has moved) 
	 * @param matchNum
	 * @param chaMove
	 * @param oppMove
	 */
	public MatchVO(int matchNum){
		super();
		
		mJsonFields.put(MATCH_NUM_FIELD, Integer.toString(matchNum));
	}
	
	/**
	 * convert match data into json string (no parent)
	 */
	@Override
	public String toJsonString(ObjectMapper mapper) {
		return toJsonNode(mapper).toString();
	}
	
	/**
	 * Convert match data into json node (no parent)
	 */
	@Override
	public JsonNode toJsonNode(ObjectMapper mapper) {
		// create the match node
		ObjectNode matchNode = mapper.createObjectNode();
		
		// add match key/value pairs into json node
		matchNode = mapToJsonNode(mJsonFields, matchNode);
		
		return matchNode;
	}
	

}
