/**
 * 
 */
package com.locomoti.android.games.roshambo.vo;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;

/**
 * VO wrapper to hold game info. 
 * This contains the default fields that will be available for communication
 * between the server and our app
 * 
 * 
 * @author 	Sergio Bernales
 * @date 	Mar 23, 2012
 *
 * Copyright 2012 Locomoti LLC. All rights reserved.
 *
 */
public class GameVO extends JsonVO {

	/**
	 * Fields of a game. These are fields that are agreed upon
	 * between the server and the device. How the server and device 
	 * actually save them is up to them. 
	 * */
	public static final String GAME_FIELD = "game";
	
	/** Field about the game */
	public static final String GAME_ID_FIELD = "gameID"; //id from the server //TODO
	public static final String STAKES_FIELD = "stakes";
	public static final String CHA_ID_FIELD = "chaID";
	public static final String OPP_ID_FIELD = "oppID";
	public static final String WINNER_FIELD = "winner";


	/** Fields about the opponent */
	public static final String OPP_NAME_FIELD = "oppName";
	public static final String OPP_TELNUM_FIELD = "oppTelnum";
	public static final String OPP_EMAIL_FIELD = "oppEmail";
	public static final String OPP_USERNAME_FIELD = "oppUsername";
	
	/** id because _id is used by both the server and the app */
	public static final String GAME_NODE_ID_FIELD = "gameNodeID"; //id from the server //TODO
	

	/**
	 * Extract the game ID from the json object
	 * @param gameNode
	 * @return
	 */
	public static String getGameNodeID(JsonNode rootNode) {
		
		// get the relevant child node
		JsonNode gameNode = rootNode.get(GAME_FIELD);
		
		return gameNode.get(JsonVO.NODE_ID_STANDARD).getTextValue();
	}


	/* (non-Javadoc)
	 * @see com.locomoti.android.games.roshambo.vo.JsonVO#toJsonString(org.codehaus.jackson.map.ObjectMapper)
	 */
	@Override
	public String toJsonString(ObjectMapper mapper) {
		// TODO Auto-generated method stub
		return null;
	}


	/* (non-Javadoc)
	 * @see com.locomoti.android.games.roshambo.vo.JsonVO#toJsonNode(org.codehaus.jackson.map.ObjectMapper)
	 */
	@Override
	public JsonNode toJsonNode(ObjectMapper mapper) {
		// TODO Auto-generated method stub
		return null;
	}
}
