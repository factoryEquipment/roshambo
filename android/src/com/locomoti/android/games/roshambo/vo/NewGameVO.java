/**
 * 
 */
package com.locomoti.android.games.roshambo.vo;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;

import android.content.ContentValues;
import android.text.TextUtils;

import com.locomoti.android.games.roshambo.provider.RoshamboContract.Game;
import com.locomoti.android.games.roshambo.provider.RoshamboContract.User;
import com.locomoti.android.games.roshambo.util.GameConstants;

/**
 * VO wrapper to hold game info. Including (possibly)
 * matches.
 * 
 * 
 * @author 	Sergio Bernales
 * @date 	Mar 23, 2012
 *
 * Copyright 2012 Locomoti LLC. All rights reserved.
 *
 */
public class NewGameVO extends GameVO {

	/**
	 * Fields of a game. These are fields that are agreed upon
	 * between the server and the device. How the server and device 
	 * actually save them is up to them. 
	 * */
	public static final String GAME_FIELD = "game";
	
	/** Field only for new games. User move always sent so make it part of the json */
	public static final String CHA_MOVE = "chaMove";
	
	/** the android sqlite id so that when we receive the http response,
	 * we now what game it is referring to*/
	public static final String ANDROID_ID_FIELD = "androidID";
	
//	/** URi that holds the android db uri to this game record*/
//	private String uri;
	
	/**
	 * Parse the new game from
	 * 
	 * @param newGameJsonTree
	 */
	public NewGameVO(JsonNode newGameJsonTree){
		super();
	}
	
	/**
	 * Create an empty game VO
	 */
	public NewGameVO(){
		super();
	}
	
	
	/* (non-Javadoc)
	 * @see com.locomoti.android.games.roshambo.vo.JsonVO#toJsonString(org.codehaus.jackson.map.ObjectMapper)
	 */
	@Override
	public String toJsonString(ObjectMapper mapper) {
		return toJsonNode(mapper).toString();
	}
	
	/* (non-Javadoc)
	 * @see com.locomoti.android.games.roshambo.vo.JsonVO#toJsonNode(org.codehaus.jackson.map.ObjectMapper)
	 */
	@Override
	public JsonNode toJsonNode(ObjectMapper mapper) {
		//create the root node
		JsonNode rootNode = mapper.createObjectNode();
		
		// add the child node
		((ObjectNode)rootNode).put(GAME_FIELD, mapper.createObjectNode());
		
		//grab that child node we just created
		ObjectNode gameNode = (ObjectNode)rootNode.path(GAME_FIELD);
		
		// insert the user key:value pairs into our new node
		gameNode = mapToJsonNode(mJsonFields, gameNode);
		
		return rootNode;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder b = new StringBuilder();
		b.append("you VS : ").append(mJsonFields.get(OPP_NAME_FIELD)).append("\n");
		b.append("stakes: " ).append(mJsonFields.get(STAKES_FIELD)).append("\n");
		b.append("You chose: ").append(GameConstants.CHARACTER_MAPPINGS.get(mJsonFields.get(CHA_MOVE)));
		
		return b.toString();
		
	}

	/**
	 * Extracts the received new game  data from the json response
	 * and put them into a ContentValues object so its ready to insert
	 * into our db
	 * 
	 * @param newGameNode
	 * @return
	 */
	public static ContentValues toContentValues(JsonNode newGameNode) {
		ContentValues values = new ContentValues();
		
		// get the relevant child node
		JsonNode gameNode = newGameNode.get(GAME_FIELD);
		
		// TODO: we can loop through the useJsonTre instread of just pulling one by one
		
		values.put(Game.GAME_NODE_ID, gameNode.get(NODE_ID_STANDARD).getTextValue());
		values.put(Game.SYNCED, gameNode.get(UPDATED_FIELD).getTextValue());
		values.put(Game.UPDATED, gameNode.get(UPDATED_FIELD).getTextValue());// same as synced date
		values.put(Game.REST_RESULT, 200);//TODO: was rest result really 200?
		values.put(Game.REST_STATE, GameConstants.REST_NEUTRAL);
	
		// some new game will not have opponent id as they might not be registered with the app
		JsonNode jsonOppID = gameNode.get(OPP_ID_FIELD);
		if(jsonOppID != null) {
			values.put(Game.GAME_OPP_ID, jsonOppID.getTextValue());
			values.put(Game.GAME_OPP_USERNAME, gameNode.get(OPP_USERNAME_FIELD).getTextValue());	
		}
		
		
		
		
		return values;
	}

	/**
	 * Extract the android sqlite db id from the json.
	 * Android ID is used to refer to the correct game during http response/request
	 * with the server. After the response we have an actual _id given by node, but
	 * until we update the android record we don't know that.
	 * 
	 * @param newGameNode
	 * @return
	 */
	public static String getAndroidID(JsonNode newGameNode) {
		
		// get the relevant child node
		JsonNode gameNode = newGameNode.get(GAME_FIELD);
		
		return gameNode.get(ANDROID_ID_FIELD).getTextValue();
	}
	
	/**
	 * Extract the android opponenet id from the json response.
	 * If the opponenet id is non existent, then we need to send a text message
	 * to the user
	 * 
	 * @param newGameNode
	 * @return
	 */
	public static boolean isOpponentRegistered(JsonNode newGameNode) {
		
		// get the relevant opponenet id node
		JsonNode oppNodeId = newGameNode.get(GAME_FIELD).get(OPP_ID_FIELD);
		
		if (oppNodeId == null || TextUtils.isEmpty(oppNodeId.getTextValue()))
			return false;
		
		return true;
	}
}
