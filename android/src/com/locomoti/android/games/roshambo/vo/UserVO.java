/**
 * 
 */
package com.locomoti.android.games.roshambo.vo;


import java.util.HashMap;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;

import android.content.ContentValues;

import com.locomoti.android.games.roshambo.provider.RoshamboContract.User;

/**
 * Value Object that stores User Info implement with an
 * in memory json tree object given by jackson json.
 * 
 * Value object to hold user information
 * 
 * @author 	Sergio Bernales
 * @date 	Mar 20, 2012
 *
 * Copyright 2012 Locomoti LLC. All rights reserved.
 *
 */
public class UserVO extends JsonVO{
	
	/**
	 * Fields of the user. These are fields that are agreed upon
	 * between the server and the device. How the server and device 
	 * actually save them is up to them. 
	 * */
	public static final String USER_FIELD = "user";
	
	public static final String USER_ID_FIELD = "_id"; //id from the server //TODO
//	public static final String _ID_FIELD = "_id"; // Android id(usually just 1)
	public static final String IMEI_FIELD = "IMEI";
	public static final String TELNUM_FIELD = "telnum";
	public static final String USERNAME_FIELD = "username";
	public static final String EMAIL_FIELD = "email";
	public static final String C2DM_ID_FIELD = "c2dmID";
	public static final String USER_NODE_ID_FIELD = "userNodeID";
	
	/**
	 * Parse the user info
	 * 
	 */
	public UserVO(JsonNode userJsonTree) {
		mJsonFields = new HashMap<String, String>();
		
		JsonNode userNode = userJsonTree.get(USER_FIELD);
		
		
		/** pull all our json fields into our hashmap */
		// GoTCHA: 'cause server ID comes in as _id
		mJsonFields.put(IMEI_FIELD, userNode.get(IMEI_FIELD).getTextValue());
		mJsonFields.put(TELNUM_FIELD, userNode.get(TELNUM_FIELD).getTextValue());
		mJsonFields.put(USERNAME_FIELD, userNode.get(USERNAME_FIELD).getTextValue());
		mJsonFields.put(EMAIL_FIELD, userNode.get(EMAIL_FIELD).getTextValue());
		mJsonFields.put(UPDATED_FIELD, userNode.get(UPDATED_FIELD).getTextValue());
		mJsonFields.put(CREATED_FIELD, userNode.get(CREATED_FIELD).getTextValue());//TODO: this may not be necessary
	}  
	
	public UserVO() {
		mJsonFields = new HashMap<String, String>();
	}
	
	/**
	 * Converts our current user data into a Json string
	 * to send to the server.
	 * @param rootNode empty JsonNode object
	 * @return
	 */
	@Override
	public String toJsonString(ObjectMapper mapper) {
		return toJsonNode(mapper).toString();
	}
	
	/**
	 * Converts our user data into a json node
	 * 
	 */
	@Override
	public JsonNode toJsonNode(ObjectMapper mapper) {
		//create the root node
		JsonNode rootNode = mapper.createObjectNode();
		
		// add the one-under user node
		((ObjectNode)rootNode).put(USER_FIELD, mapper.createObjectNode());
		
		// grab the user node to start inserting
		ObjectNode userNode = (ObjectNode)rootNode.path(USER_FIELD);
		
		// insert the user key:value pairs into our new node
		userNode = mapToJsonNode(mJsonFields, userNode);
		
		return rootNode;
	}
	
	/**
	 * Converts our user data into a ContentValues object
	 * used to enter into the sqlite db
	 * 
	 * @param userJsonTree
	 * @return
	 */
	public static ContentValues toContentValues(JsonNode userJsonTree){
		ContentValues values = new ContentValues();
		
		JsonNode userNode = userJsonTree.get(USER_FIELD);
		
		// TODO: we can loop through the useJsonTre instread of just pulling one by one
		
		values.put(User._ID, User.DEFAULT_ID);
		values.put(User.USER_NODE_ID, userNode.get(USER_ID_FIELD).getTextValue());
		values.put(User.USER_USERNAME, userNode.get(USERNAME_FIELD).getTextValue());
		values.put(User.USER_EMAIL, userNode.get(EMAIL_FIELD).getTextValue());
		values.put(User.SYNCED, userNode.get(UPDATED_FIELD).getTextValue());
		values.put(User.UPDATED, userNode.get(UPDATED_FIELD).getTextValue());// same as synced date
		
		return values;
	}
}
