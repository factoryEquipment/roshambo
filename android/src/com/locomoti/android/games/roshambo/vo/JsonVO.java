/**
 * 
 */
package com.locomoti.android.games.roshambo.vo;

import java.util.HashMap;
import java.util.Map;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;

import android.content.ContentValues;
import android.text.TextUtils;

/**
 * Interface for a wrapper of json data. Every
 * VO that implements this needs define a printout
 * of the Json data it contains so to send over the network
 * 
 * @author 	Sergio Bernales
 * @date 	Mar 22, 2012
 *
 * Copyright 2012 Locomoti LLC. All rights reserved.
 *
 */
public abstract class JsonVO {
	
	/** The standard Node.js id field. This is constant*/
	static public final String NODE_ID_STANDARD = "_id";

	/** Timestamp to define when the request was sent/created*/
	static public final String TIMESTAMP_FIELD = "timestamp";
	
	/** Common json keys to all resources*/
	static public final String UPDATED_FIELD = "updated";
	static public final String CREATED_FIELD = "created";
	
	/** Map that will store all our json key:values pairs */
	protected HashMap<String, String> mJsonFields;
	
	ContentValues mContentValues;
	
	/**
	 * Default constructor
	 */
	public JsonVO(){
		mJsonFields = new HashMap<String, String>();
	}
	
	/** Common converter methods our children need to implement*/
	abstract public String toJsonString(ObjectMapper mapper);
	abstract public JsonNode toJsonNode(ObjectMapper mapper);

	/** Map setter and getters */
	public void setField(String key, String value) {
		mJsonFields.put(key, value);
	}
	
	public String getField(String key) {
		return mJsonFields.get(key);
	}
	
	/**
	 * Transfer map key/value pairs into the JSON node
	 * @param node
	 */
	protected static ObjectNode mapToJsonNode(HashMap<String, String> JsonFields, 
										ObjectNode node){
		for(Map.Entry<String, String> cursor: JsonFields.entrySet()){
			if(cursor.getValue() != null && !TextUtils.isEmpty(cursor.getValue()))
				node.put(cursor.getKey(), cursor.getValue());
		}
		
		return node;
	}
	
	/**
	 * A check to see if the field exists in our map
	 * @param field
	 * @return
	 */
	public boolean containsField(String key){
		return mJsonFields.containsKey(key);
	}

	public ContentValues getContentValues() {
		return mContentValues;
	}

	public void setContentValues(ContentValues mContentValues) {
		this.mContentValues = mContentValues;
	}
}
