/**
 * 
 */
package com.locomoti.android.games.roshambo.vo;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;

import com.locomoti.android.games.roshambo.provider.RoshamboContract.Game;
import com.locomoti.android.games.roshambo.provider.RoshamboContract.Matches;
import com.locomoti.android.games.roshambo.provider.RoshamboContract.User;
import com.locomoti.android.games.roshambo.util.GameConstants;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * @author 	Sergio Bernales
 * @date 	Apr 19, 2012
 *
 * Copyright 2012 Locomoti LLC. All rights reserved.
 *
 */
public class MoveVO extends JsonVO {
	private static final String ROOT_FIELD = "match";
	
	public static final String MOVE_FIELD = "move";
	public static final String GAME_NODE_ID_FIELD = GameVO.GAME_NODE_ID_FIELD;
	public static final String USER_NODE_ID_FIELD = UserVO.USER_NODE_ID_FIELD;
	
	/**
	 * Get a json string fromt he values given. there are only three for a move
	 * my node id, my move, and the game id
	 * 
	 * @param mapper
	 */
	@Override
	public String toJsonString(ObjectMapper mapper) {
		JsonNode rootNode = mapper.createObjectNode();
		
		// add the child node
		((ObjectNode)rootNode).put(ROOT_FIELD, mapper.createObjectNode());
		
		//grab that child node we just created
		ObjectNode matchNode = (ObjectNode)rootNode.path(ROOT_FIELD);
		
		// insert the user key:value pairs into our new node
		matchNode.put(MOVE_FIELD, mContentValues.getAsString(Matches.MATCH_MY_MOVE));
		matchNode.put(GAME_NODE_ID_FIELD, mContentValues.getAsString(Game.GAME_NODE_ID));
		matchNode.put(USER_NODE_ID_FIELD, mContentValues.getAsString(User.USER_NODE_ID));

		
		return rootNode.toString();
	}

	/* (non-Javadoc)
	 * @see com.locomoti.android.games.roshambo.vo.JsonVO#toJsonNode(org.codehaus.jackson.map.ObjectMapper)
	 */
	@Override
	public JsonNode toJsonNode(ObjectMapper mapper) {
		// TODO Auto-generated method stub
		return null;
	}

}
