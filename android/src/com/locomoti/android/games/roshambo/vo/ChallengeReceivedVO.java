/**
 * 
 */
package com.locomoti.android.games.roshambo.vo;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;

import android.content.ContentValues;
import android.os.Bundle;

import com.locomoti.android.games.roshambo.provider.RoshamboContract.Game;
import com.locomoti.android.games.roshambo.util.GameConstants;

/**
 * VO object to work with the data that comes in
 * from teh C2DM messgaes. this will parse and store it
 * as necessary.
 * 
 * @author 	Sergio Bernales
 * @date 	Apr 18, 2012
 *
 * Copyright 2012 Locomoti LLC. All rights reserved.
 *
 */
public class ChallengeReceivedVO extends GameVO {

	public static final String CHA_USERNAME_FIELD = "chaUsername";
	public static final String GAME_NODE_ID_FIELD = "gameNodeID";
	
	/**
	 * Extracts the received new game  data from the json response
	 * and put them into a ContentValues object so its ready to insert
	 * into our db
	 * 
	 * @param newGameNode
	 * @return
	 */
	public static ContentValues toContentValues(Bundle challengeReceived) {
		ContentValues values = new ContentValues();

		// TODO: we can loop through the useJsonTre instread of just pulling one by one
		
		values.put(Game.GAME_NODE_ID, challengeReceived.getString(NODE_ID_STANDARD));
		values.put(Game.GAME_STAKES, challengeReceived.getString(STAKES_FIELD));
		// the challenger in this case is my opponent
		values.put(Game.GAME_OPP_ID, challengeReceived.getString(CHA_ID_FIELD));
		values.put(Game.GAME_OPP_USERNAME, challengeReceived.getString(CHA_USERNAME_FIELD));
//		values.put(Game.UPDATED, challengeReceived.getString(OPP_ID_FIELD)); this is me
		
		values.put(Game.GAME_CREATED, challengeReceived.getString(CREATED_FIELD));
		
		
		return values;
	}
	
	/* (non-Javadoc)
	 * @see com.locomoti.android.games.roshambo.vo.JsonVO#toJsonString(org.codehaus.jackson.map.ObjectMapper)
	 */
	@Override
	public String toJsonString(ObjectMapper mapper) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.locomoti.android.games.roshambo.vo.JsonVO#toJsonNode(org.codehaus.jackson.map.ObjectMapper)
	 */
	@Override
	public JsonNode toJsonNode(ObjectMapper mapper) {
		// TODO Auto-generated method stub
		return null;
	}

}
