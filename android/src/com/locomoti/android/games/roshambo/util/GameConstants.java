/**
 * 
 */
package com.locomoti.android.games.roshambo.util;

import java.util.HashMap;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.locomoti.android.games.roshambo.provider.RoshamboContract.User;

/**
 * @author 	Sergio Bernales
 * @date 	Mar 26, 2012
 *
 * Copyright 2012 Locomoti LLC. All rights reserved.
 *
 */
public class GameConstants {
	public static final String URI = "URI";
	
	public static final String GOOGLE_PLAY_APP_LINK = "https://play.google.com/store/apps/details?id=com.storm8.vampires12";
	
	public static final String COWBOY_MOVE 	= "1";
	public static final String BEAR_MOVE	= "2";
	public static final String NINJA_MOVE 	= "4";
	
	
	public static HashMap<String, String> CHARACTER_MAPPINGS;
	public static final String COWBOY = "cowboy";
	public static final String BEAR = "bear";
	public static final String NINJA = "ninja";
	
	
	static {
		CHARACTER_MAPPINGS = new HashMap<String, String>();
		CHARACTER_MAPPINGS.put(COWBOY_MOVE, COWBOY);
		CHARACTER_MAPPINGS.put(BEAR_MOVE, BEAR);
		CHARACTER_MAPPINGS.put(NINJA_MOVE, NINJA);
	}
	
	public static final short COWBOY_MOVE_NUM = 0x01;
	public static final short BEAR_MOVE_NUM 	= 0x02; 
	public static final short NINJA_MOVE_NUM = 0x04;
	
	/** TRUE False values*/
	public static final short SHORT_TRUE = 0x01;
	public static final short SHORT_FALSE = 0x00;
	
	/** Game states */
	public static final short STATE_ACTIVE = 0x00;
	public static final short STATE_WON = 0x01;
	public static final short STATE_LOST = 0x02;
	
	public static final short REST_NEUTRAL = 0x00;
	public static final short REST_GETTING = 0x01;
	public static final short REST_PUTTING = 0x02;
	public static final short REST_POSTING = 0x03;
	public static final short REST_DELETING = 0x01;
	
	public static final short HTTP_NULL = 0x00;
	public static final short HTTP_SUCCESS = 200;
	
	/*********************************************
	 *********************************************
	 * Some global static methods
	 *********************************************
	 ********************************************* 
	 */
	
	public static long getTimestamp(){
		return System.currentTimeMillis()/1000;
	}
	
	public static String createRandomResourceID(long timeA){
		long timeB = getTimestamp()<<16;
		
		long timeC = timeA | timeB;
		
		String t = Long.toHexString(timeC);
		return "/" + t ;
	}
	
	public static String getUserNodeID(Context context){
		SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
		return pref.getString(User.USER_NODE_ID, null);
	}
	
	public static String getUsername(Context context){
		SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
		return pref.getString(User.USER_USERNAME, null);
	}
}
