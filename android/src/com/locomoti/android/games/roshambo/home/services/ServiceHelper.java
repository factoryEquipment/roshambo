/**
 * 
 */
package com.locomoti.android.games.roshambo.home.services;

import com.locomoti.android.games.roshambo.vo.UserVO;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;



/**
 * Provides helpful methods that packages up an intent and send
 * of to the service. This provides a thin layer of abstraction
 * between the UI and the our Service.
 * Could include all of the following attribtues (googlio 2010)
 *	� Check if the method is already pending
 *	� Create the request Intent
 *	� Add the operation type and a unique request id
 *	� Add the method specific parameters
 *	� Add the binder callback
 *	� Call startService(Intent)
 *	� Return the request id 
 * 
 * @author 	Sergio Bernales
 * @date 	Mar 15, 2012
 *
 * Copyright 2012 Locomoti LLC. All rights reserved.
 *
 */
public class ServiceHelper  {
	private static final String TAG = "ServiceHelper";
	
	/**
	 * Sends a create user request to our service
	 */
	public void createUser(Context context, ContentValues newUserValues) {
		
		// Create our intent to send to service
		Intent i = new Intent(context, RestService.class);
		
		// put in the new user values
		i.putExtra(RestService.EXTRA_NEW_USER, newUserValues);
		
		// intent action so that the service know what/how to process
		i.putExtra(RestService.EXTRA_ACTION_TYPE, RestService.ACTION_CREATE_USER);
		
		// Launch the service
		context.startService(i);
	}

	/**
	 * Sends a create new game intent to our
	 * service intent
	 * 
	 * @param context
	 * @param values
	 */
	public void createNewGame(Context context, ContentValues newGameValues) {
		// Create our intent to send to service
		Intent i = new Intent(context, RestService.class);
		
		// put in the new user values
		i.putExtra(RestService.EXTRA_NEW_GAME, newGameValues);
		
		// intent action so that the service know what/how to process
		i.putExtra(RestService.EXTRA_ACTION_TYPE, RestService.ACTION_CREATE_NEW_GAME);
		
		// Launch the service
		context.startService(i);
	}

	/**
	 * Submits a move for a particular game. Any time we want to submit a
	 * move for a game, we use this call. 
	 * 
	 * @param context
	 * @param values
	 */
	public void submitMatchMove(Context context, ContentValues values) {
		// Create our intent to send to service
		Intent i = new Intent(context, RestService.class);
		
		// put in the new user values
		i.putExtra(RestService.EXTRA_MATCH_MOVE, values);
		
		// intent action so that the service know what/how to process
		i.putExtra(RestService.EXTRA_ACTION_TYPE, RestService.ACTION_SUBMIT_MATCH_MOVE);
		
		// Launch the service
		context.startService(i);
		
	}	
}
