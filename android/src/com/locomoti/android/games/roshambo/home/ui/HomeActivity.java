package com.locomoti.android.games.roshambo.home.ui;


import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.locomoti.android.games.roshambo.R;
import com.locomoti.android.games.roshambo.fragments.CharacterSelectionFragment.CharacterSelectionInterface;
import com.locomoti.android.games.roshambo.fragments.OpponentSelectionFragment.OnOpponentSelectedListener;
import com.locomoti.android.games.roshambo.fragments.StakesFragment;
import com.locomoti.android.games.roshambo.home.adapters.HomeFragmentPagerAdapter;
import com.locomoti.android.games.roshambo.provider.RoshamboAsyncQueryHandler;
import com.locomoti.android.games.roshambo.provider.RoshamboAsyncQueryHandler.AsyncQueryListener;
import com.locomoti.android.games.roshambo.provider.RoshamboContract.Game;
import com.locomoti.android.games.roshambo.provider.RoshamboContract.Matches;
import com.locomoti.android.games.roshambo.util.GameConstants;
import com.locomoti.android.games.roshambo.vo.NewGameVO;


/**
 * Home activity launched when entering app. This app
 * has several frgaments it loads and interfaces with.
 * See {@link HomeViewPager}
 * 
 * 
 * @author Sergio Bernales
 *
 */
public class HomeActivity extends FragmentActivity implements OnOpponentSelectedListener, 
						ViewPager.OnPageChangeListener, AsyncQueryListener, CharacterSelectionInterface{
	public static final String TAG = "HomeActivity";
	
	/** HOlds that mode of our HomeActivity. Since the home activity contains
	 * the fragments to start games/challenges, the fragments that are dependent on
	 * previous fragment selections (like oppponent should be selecged before entering
	 * what kind of challenge), then we need to hold this state */
	private static int GAME_MODE;
	
	/** Out three modes our home activity can be in */
	private static final int MODE_INITIAL = 0;
	private static final int MODE_OPPONENT_SELECTED = 1;
	private static final int MODE_STAKES_ENTERED = 2;
	private static final int MODE_CHARACTER_SELECTED = 3;
	
	/** Async query call tokens */
	private static final int TOKEN_SUBMIT_GAME = 1;
	private static final int TOKEN_NEW_GAME_PROCESSED = 1;
	
	/** Observer to game data wile requests happen. basically
	 * in charge of displaying results or http request plus
	 * getting rid of loading animation*/
	private GameRequestObserver mGameRequestObserver;
	
	ViewPager mHomeViewPager;
	FragmentPagerAdapter mFragmentAdapter;
	
	RoshamboAsyncQueryHandler mAsyncQUeryHandler;
	
	NewGameVO mNewGameVO;
	
	ContentValues mNewGameValues;

	private Uri mNewGameUri;
	
	
	
	/**
	 * Reset our game data values as we can now let the user start a new challenge
	 * 
	 */
	private void reset() {
		mNewGameValues = new ContentValues();
		mNewGameVO = new NewGameVO();
		GAME_MODE = MODE_INITIAL;
		// TODO: clear out the form fields of the layouts as well
	}
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        
        // set our mode to initial
        GAME_MODE = MODE_INITIAL;
        
        // create a ContentValue object that will be re used
        mNewGameValues = new ContentValues();
        
        // Remove the title and make fulls screen
        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
//                                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        
        // Set the view to out Home View Pager
        setContentView(R.layout.home_view_pager);
        
        // get our view pager 
        mHomeViewPager = (ViewPager) findViewById(R.id.home_view_pager);
        
        // create a new view pager adapter
        mFragmentAdapter = new HomeFragmentPagerAdapter(this, mHomeViewPager);
        
        // set view pager's adapter to our custom adapter
        mHomeViewPager.setAdapter(mFragmentAdapter);
        
        // set the view pager's page listner to activity
        mHomeViewPager.setOnPageChangeListener(this);

        // The main fragment in the home activity is the second one
        mHomeViewPager.setCurrentItem(HomeFragmentPagerAdapter.HOME_SCREEN_FRAG_POS);
        
        //Create a new game VO
        mNewGameVO = new NewGameVO();
        
		// content observer that listens responses of game requests that have been sent the server
        // like game creation, or move submission
        mGameRequestObserver = new GameRequestObserver(this, new Handler());
    }
    
    /* (non-Javadoc)
     * @see android.support.v4.app.FragmentActivity#onResume()
     */
    @Override
    protected void onStart() {
    	super.onStart();
    	
    	// register oue listener
		getContentResolver().registerContentObserver(Game.buildGameRequestProcessedURI(), true, mGameRequestObserver);
    }

    /* (non-Javadoc)
     * @see android.support.v4.app.FragmentActivity#onStop()
     */
    @Override
    protected void onStop() {
    	// TODO Auto-generated method stub
    	super.onStop();
    	
    	//HomeActivity unregister
    	if (mGameRequestObserver != null)
    		getContentResolver().unregisterContentObserver(mGameRequestObserver);
    }
	/**
	 * Called when an opponenet is selected in the
	 * Opponenet Selection Fragment
	 * TODO: email
	 */
	@Override
	public void onOpponentSelected(String name, String telnum) {
		Toast t = Toast.makeText(this, "Opponent selected! : " + name + " : " + telnum, 5000);
		t.show();
		
		// change the mode to opponenet selected
		GAME_MODE = MODE_OPPONENT_SELECTED;
		
		// add the new fields to our gameVO
		mNewGameValues.put(Game.GAME_OPP_NAME, name);
		mNewGameValues.put(Game.GAME_OPP_TELNUM, telnum);
		
		mNewGameVO.setField(NewGameVO.OPP_NAME_FIELD, name);
		mNewGameVO.setField(NewGameVO.OPP_TELNUM_FIELD, telnum);
		
		// notify the stakes fragment that we have changed modes
		//particularly we have selected an oponnent
		StakesFragment stakesFragment = (StakesFragment) mFragmentAdapter.getItem(HomeFragmentPagerAdapter.STAKES_FRAG_POS);
		stakesFragment.onModeChanged(GAME_MODE);
	}
	
	/**
	 * Interface that fragment implmenet to get notifications that
	 * the mode has changed. LIke when the use selected an opponenet,
	 * the other fragment need to be aware of this.
	 * 
	 * @author 	Sergio Bernales
	 * @date 	Mar 26, 2012
	 *
	 * Copyright 2012 Locomoti LLC. All rights reserved.
	 *
	 */
	public interface OnModeChangedListener {
		void onModeChanged(int newMode);
	}

	/**
	 * Listener that handles a character being selected by the user
	 */
	@Override
	public void onCharacterSelected(String character) {
		// change to new mode
		GAME_MODE = MODE_CHARACTER_SELECTED;
		
		// add character value our content
		mNewGameValues.put(Matches.MATCH_MY_MOVE, character);
		
		mNewGameVO.setField(NewGameVO.CHA_MOVE, character);
		mNewGameVO.getField(NewGameVO.CHA_MOVE);
	}
	
	/**
	 * Submit game button has been clicked! Handle it. Send if offffff
	 * to the content provider asynchornously
	 */
	public void onSubmit(View v) {
		Log.d(TAG, "Submitting our game!");
		
		// create asyn query handler if one not available
		if (mAsyncQUeryHandler == null)
			mAsyncQUeryHandler = new RoshamboAsyncQueryHandler(getContentResolver(), this);
		
		// send insert call to content provider
		mAsyncQUeryHandler.startInsert(TOKEN_SUBMIT_GAME, mNewGameVO, Game.buildNewGameURI(), mNewGameValues);
		
		// reset values
		reset();
		
		// move us back to the home screen
		mHomeViewPager.setCurrentItem(HomeFragmentPagerAdapter.HOME_SCREEN_FRAG_POS);
		
	}
	
//	/**
//	 * Sends true if all required fields of a new game have been filled out.
//	 */
//	public boolean isNewGameStepsCompleted() {
////		if(!mNewGameVO.containsField(GameVO.OPP_NAME) )
//		return true;//todo
//			
//	}


	/********************************
	 ********************************
	 * All Async query handlers
	 ********************************
	 ********************************
	 */

	/* (non-Javadoc)
	 * @see com.locomoti.android.games.roshambo.provider.RoshamboAsyncQueryHandler.AsyncQueryListener#onQueryComplete(int, java.lang.Object, android.database.Cursor)
	 */
	@Override
	public void onQueryComplete(int token, Object cookie, Cursor cursor) {
		Log.d(TAG, "Query completed: " + Integer.toString(token));
		
		switch(token){
			case TOKEN_NEW_GAME_PROCESSED:
				Log.d(TAG, "Game received");
				
				if(cursor != null && cursor.getCount() == 1){
					cursor.moveToFirst();
					String oppUsername = cursor.getString(NewGameQuery.GAME_OPP_USERNAME);
					
					// if no username, opponenet was never registered, send text message
					if (TextUtils.isEmpty(oppUsername)){
						sendChallengeSMS(cursor.getString(NewGameQuery.GAME_OPP_TELNUM), cursor.getString(NewGameQuery.GAME_STAKES));
					}
				}
				
				Toast t = Toast.makeText(this, "game created", 1500);
				
				return;
				
			default:
				return;
		}
	}


	/* (non-Javadoc)
	 * @see com.locomoti.android.games.roshambo.provider.RoshamboAsyncQueryHandler.AsyncQueryListener#onInsertComplete(int, java.lang.Object, android.net.Uri)
	 */
	@Override
	public void onInsertComplete(int token, Object cookie, Uri uri) {
		mNewGameUri = uri;
		Log.d(TAG, "Insert completed ");
	}


	/* (non-Javadoc)
	 * @see com.locomoti.android.games.roshambo.provider.RoshamboAsyncQueryHandler.AsyncQueryListener#onUpdateComplete(int, java.lang.Object, int)
	 */
	@Override
	public void onUpdateComplete(int token, Object cookie, int result) {
		// TODO Auto-generated method stub
		
	}


	/**
	 * @return
	 */
	public NewGameVO getNewGameVO() {
		// TODO Auto-generated method stub
		return mNewGameVO;
	}
	
	/**
	 * Will observe when game request has been submitted
	 * and we have a response. 
	 * ONLY ONE OBSERVATION should be enabled .
	 * This also get rid of the loading animation which should
	 * be showing as say a game move is being submitted
	 * 
	 * @author 	Sergio Bernales
	 * @date 	Mar 19, 2012
	 *
	 * Copyright 2012 Locomoti LLC. All rights reserved.
	 *
	 */
	private class GameRequestObserver extends ContentObserver {

		Context mContext;
		/**
		 * @param handler
		 */
		public GameRequestObserver(Context context, Handler handler) {
			super(handler);
			
			mContext = context;
		}
		
		/**
		 * metod that gets called after the user has been created 
		 * in the server. Proceed to next activity
		 * 
		 * @see android.database.ContentObserver#onChange(boolean)
		 */
		@Override
		public void onChange(boolean selfChange) {
			Log.d(TAG, "game has changed");
			

			HomeActivity.this.mAsyncQUeryHandler.startQuery(TOKEN_NEW_GAME_PROCESSED, null, mNewGameUri, NewGameQuery.PROJECTION,
					null, null, null);
		}

		@Override
		public boolean deliverSelfNotifications() {
			return true;
		}	
	}
	
	/**
	 * Query columns for game retrieval
	 */
    private interface NewGameQuery {
        String[] PROJECTION = {
            Game.GAME_OPP_USERNAME,
            Game.GAME_STAKES, 
            Game.GAME_OPP_TELNUM
        };

        int GAME_OPP_USERNAME = 0;
        int GAME_STAKES = 1;
        int GAME_OPP_TELNUM = 2;
    }

	/********************************
	 ********************************
	 * View Pager event listener
	 ********************************
	 ********************************
	 */
	
	@Override
	public void onPageScrollStateChanged(int arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}


	/**
	 * Some logic based on the page the user has reached. It takes care of
	 * handling validation and among other things
	 */
	@Override
	public void onPageSelected(int pageNum) {
		Log.d(TAG, Integer.toString(pageNum));
		
		// hide the soft keyboard
		if(getCurrentFocus() != null){
			InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
		}

		
		switch(pageNum) {
			case HomeFragmentPagerAdapter.STAKES_FRAG_POS:
				// if opponent never selected, cant go here
				if(GAME_MODE == MODE_INITIAL){
					Toast.makeText(this, "Select OPPONENT First!!", 5000).show();
					
					mHomeViewPager.setCurrentItem(HomeFragmentPagerAdapter.OPP_SEL_FRAG_POS);
					return;
				}
				
				return;
				
			case HomeFragmentPagerAdapter.CHAR_SEL_FRAG_POS:
				
				// at this point, the stakes should have been entered, check that
				StakesFragment stakesFrag = (StakesFragment) mFragmentAdapter.getItem(HomeFragmentPagerAdapter.STAKES_FRAG_POS);
				String stakes = stakesFrag.getStakes();
				
				// if empty, then return to stakes screen. This is required!
				if(TextUtils.isEmpty(stakes)){
					Toast.makeText(this, "You need to enter stakes!!", 5000).show();
					
					mHomeViewPager.setCurrentItem(HomeFragmentPagerAdapter.STAKES_FRAG_POS);
					return;
				}
				
				// if not empty, we are in the last mode, ready to select character
				GAME_MODE = MODE_STAKES_ENTERED;
				
				// insert the stakes into our newGameVO
				mNewGameValues.put(Game.GAME_STAKES, stakes);
				
				mNewGameVO.setField(NewGameVO.STAKES_FIELD, stakes);
				
				return;
				
			default:
				return ;
			
		}
	}
	
//	/************************************ 
//	 ************************************
//	 *
//	 * Broadcast receiver of the result
//	 * of checking the opponent existence
//	 * 
//	 ************************************
//	 ************************************
//	 */
//	public class OpponentCheckResponseReceiver extends BroadcastReceiver {
//	   public static final String ACTION_RESP =
//	      "com.locomoti.user.checked";
//	 
//	   @Override
//	    public void onReceive(Context context, Intent intent) {
//	      Log.d(TAG, "We received something");
//	    }
//	}
	
	/**
	 * Sends a mtext message to an opponenet
	 * that is not registered with the application
	 * @param newGameNode
	 */
	private void sendChallengeSMS(String number, String stakes) {
        Intent smsIntent = new Intent(Intent.ACTION_VIEW); 
        
        smsIntent.setType("vnd.android-dir/mms-sms"); 
        smsIntent.putExtra("address", number); 
        
        //build message body
        StringBuilder sb = new StringBuilder();
        sb.append(GameConstants.getUsername(this)).append(" challenged you for \"").append(stakes).append("\"");
        sb.append(" To fight back, use Bear Fight! https://play.google.com/store/apps/details?id=com.storm8.vampires12");
        
        smsIntent.putExtra("sms_body",sb.toString());
        
        startActivity(smsIntent); 
	}

	/* (non-Javadoc)
	 * @see com.locomoti.android.games.roshambo.fragments.CharacterSelectionFragment.CharacterSelectionInterface#getGameSummary()
	 */
	@Override
	public String getGameSummary() {
		// TODO Auto-generated method stub
		return mNewGameVO.toString();
	}
	
}

	