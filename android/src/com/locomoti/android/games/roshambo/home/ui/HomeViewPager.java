/**
 * 
 */
package com.locomoti.android.games.roshambo.home.ui;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;

/**
 * In charge of setting up the different Fragments (in this case they correspond
 * each to a comic panel) for the home/default activity. 
 * This includes:
 * 		-Open Games Fragment
 * 		-Home Screen Fragment
 * 		the 3/4 Create-A-Challenge Fragments
 * 			-Choose Opponent Fragment
 * 			-Specify Stakes Fragment
 * 			-Choose a character Fragment
 * 			-Opponent Analytics (premium mode only)
 * 
 * @author Sergio Bernales
 *
 */
public class HomeViewPager extends ViewPager {

	Context mContext;
	
	/**
	 * Constructor to start a new HomeViewPager
	 * 
	 * @param context
	 */
	public HomeViewPager(Context context, AttributeSet attr) {
		super(context, attr);
		
		mContext = context; 

	}
}
