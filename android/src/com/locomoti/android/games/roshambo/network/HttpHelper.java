/**
 * 
 */
package com.locomoti.android.games.roshambo.network;

/**
 * Interface defining a bunch of constants that are general to
 * the HTTP communication protocol
 * 
 * @author 	Sergio Bernales
 * @date 	Mar 22, 2012
 *
 * Copyright 2012 Locomoti LLC. All rights reserved.
 *
 */
public interface HttpHelper {
	
	/** Header properties */
	static final String USER_AGENT_HEADER = "User-Agent";
	static final String USER_AGENT = "Roshambo";
	static final String CONTENT_TYPE_HEADER = "Content-Type";
	static final String APPLICATION_JSON = "application/json";
	static final String AUTHORIZATION_HEADER = "Authorization";
	
	static final String PROTOCOL = "https";
	static final String HOST = "localhost";
	static final String FORWARD_SLASH = "/";
	static final String REST_VERSION = "/v1";
	
	// THE HTTP methods
	static final String HTTP_GET = "GET";
	static final String HTTP_POST = "POST";
	static final String HTTP_PUT = "PUT";
	static final String HTTP_DELETE = "DELETE";
}
