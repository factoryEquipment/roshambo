// ==========================================================================
// The M-Project - Mobile HTML5 Application Framework
// Generated with: Espresso 
//
// Project: Mtest 
// ==========================================================================

var Mtest  = Mtest || {};

Mtest.app = M.Application.design({

    /* Define the entry/start page of your app. This property must be provided! */
    entryPage : 'dashboard',

    dashboard : Mtest.DashboardPage
});
