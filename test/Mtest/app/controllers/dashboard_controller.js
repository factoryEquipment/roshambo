// ==========================================================================
// The M-Project - Mobile HTML5 Application Framework
// Generated with: Espresso 
//
// Project: Mtest
// Controller: DashboardController
// ==========================================================================

Mtest.DashboardController = M.Controller.extend({

    items: null,

    /*
    * Sample function
    * To handle the first load of a page.
    */
    init: function(isFirstLoad) {
        if(isFirstLoad) {
            this.set('items', [{
                icon: 'theme/images/people.png',
                label: 'USERS',
                value: 'createUser',   
                events: {
                    tap: {
                        target: this,
                        action: function() {
									console.log('hellow rold');
									this.switchToPage(Mtest.UserListPage);
                    		}
                    }
                }
            },
            {
                icon: 'theme/images/restricted.png',
                label: 'Challenge',
                value: 'challenge',
            },
            {
                icon: 'theme/images/restricted.png',
                label: 'Play',
                value: 'play'
            },
            {
                icon: 'theme/images/restricted.png',
                label: 'Stats',
                value: 'stats'
            }]);
        }
    },

    switchToExamplePage: function() {
        /* switch to a page called 'examplePage' */
        this.switchToPage('examplePage');
    }

});
