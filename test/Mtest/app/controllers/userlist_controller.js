// ==========================================================================
// The M-Project - Mobile HTML5 Application Framework
// Generated with: Espresso 
//
// Project: Mtest
// Controller: userlist_controller
// ==========================================================================

Mtest.UserListController = M.Controller.extend({

	users: null,
	mode: 'list', //usually list, but sometimes challenge!!(to pick opponent)
	
	/*
		Always request for the list of users. Good way to test the api
		and also refresh the page
	*/
	init: function(isFirstLoad) {
		console.log('Hi there');
		M.Request.init({
			url: '/node/v1/users',
			method: 'GET',
			isJSON: YES,
			callbacks: {
				error: {
					target: this,
					action: function(xhr, msg) {
				        console.log('Error: ' + msg);
				    }
				},
				success: {
					target: this,
					action: 'receiveUserList'
				}
			}
		}).send();
    },
	
	/*
		Just to pupulate the list of users
	*/
	receiveUserList: function(data, msg, xhr){
		this.set('users', data.users);
	},
	
	/*
		Enter here when user is wanting to challenge someone 
		(basically, setup list mode and swith to list page)
	*/
	challenge: function() {
		this.mode = 'challenge';
		this.switchToPage(Mtest.UserListPage);
	},
	
	/*
		User was clicked, now do something!
		ether go to edit mode or if we were in challenge mode, go to select move page
	*/
	userItemClicked: function(m_id, nameId){
		if(this.mode == 'challenge') {
			if(nameId == Mtest.UserFormController.user._id){
				alert("you challenged yourself? come on now");
				return;
			}
				
			// go to challenge screen. send challenger and opponent
			Mtest.ChallengeController.initChallenge(Mtest.UserFormController.user._id, Mtest.UserFormController.user._id, nameId);
			return;
		}
		
		Mtest.UserFormController.edit(m_id, nameId);
		// alert("Just pcking someone4 ! id " + m_id + " and nameId " + nameId);
	}
	
});
