// ==========================================================================
// The M-Project - Mobile HTML5 Application Framework
// Generated with: Espresso 
//
// Project: Mtest
// Controller: challenge_controller
// ==========================================================================

Mtest.ChallengeController = M.Controller.extend({

	/*
		Challenger and opponent ID's
	*/
	cid: null,
	oid: null,
	player: null,

	/*
		A player(id) is
	*/
   initChallenge: function(_player, _cid, _oid) {
		this.cid = _cid;
		this.oid = _oid;
		this.player = _player;

		Mtest.UserListController.switchToPage(Mtest.ChallengePage);
   },

	choose: function() {
		var buttonGroup = M.ViewManager.getView(Mtest.ChallengePage, 'choices');
		alert(buttonGroup.getActiveButton().value);
	}
	
});
