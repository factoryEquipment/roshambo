// ==========================================================================
// The M-Project - Mobile HTML5 Application Framework
// Generated with: Espresso 
//
// Project: Mtest
// Controller: userform_controller
// ==========================================================================

Mtest.UserFormController = M.Controller.extend({

   // User we are currently modifying
	user: null,
	isNewUser: YES,

	/*
		Two ways to go into FORM page, To create new user or edit existing
	*/
	create:	function(){
		this.isNewUser = YES;
		
		// new empty user
		alert('creatingggggg!');
		this.set('user', null);
		this.set('user', {
			IMEI: '',
			telnum: '',
			fname: '',
			lname: '',
		});
		this.switchToPage(Mtest.UserFormPage);
	},
	
	edit: function (m_id, nameId) {//we will come in here when click on the list
		this.isNewUser = NO;
		
		// request user info!
		M.Request.init({
			url: '/node/v1/users/'+nameId,
			method: 'GET',
			isJSON: YES,
			onSuccess: function(data, msg, xhr) {
				Mtest.UserFormController.set('user', data.user);
				Mtest.UserListController.switchToPage(Mtest.UserFormPage);
			},
			onError: function(xhr, msg) {
				alert('ERROR!');
				console.log('Error: ' + msg);
			}
		}).send();
	},
	
	
	clearForm: function() {
       var form = M.ViewManager.getView(Mtest.UserFormPage.content,'form')
       form.clearForm();
   },

	save: function() {
		console.log('SAVINGG!!');
		
		// Creating a new user
		alert(JSON.stringify({ 
			user: this.user
		}));
		if(this.isNewUser == YES){
			M.Request.init({
				url: '/node/v1/users',
				method: 'POST',
				isJSON: YES,
				data: JSON.stringify({ 
					user: this.user
				}),
				onSuccess: function(data, msg, xhr) {
					alert(JSON.stringify(data));
					Mtest.UserFormController.isNewUser = NO;//not a new user anymore
				},
				onError: function(xhr, msg) {
					alert('ERROR!');
					console.log('Error: ' + msg);
				}
			}).send();
			return;
		}
		
		//else we are just moidyfing an old user. save it here with a put
		alert('SAVING (update) has not been implemented right now FOOL!')
		//TODO: impement saving, this will happen when editing user
    }	
});
