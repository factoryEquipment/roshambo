// ==========================================================================
// The M-Project - Mobile HTML5 Application Framework
// Generated with: Espresso 
//
// Project: Mtest
// View: userlist_page
// ==========================================================================

// m_require('app/views/userlist_item.js');
Mtest.UserListPage = M.PageView.design({

/* Use the 'events' property to bind events like 'pageshow' */
	events: {
		pageshow: {
			target: Mtest.UserListController,
			action: 'init'
		}
	},

	childViews: 'header content footer',

	header: M.ToolbarView.design({
		childViews: 'back label create',
		
		back: M.ButtonView.design({
			anchorLocation: M.LEFT,
			value: 'Back',
         icon: 'arrow-l',
         internalEvents: {
         	tap: {
            	action: function() {
						history.back(-1);
            	}
         	}
			}			
		}),		
		
		create: M.ButtonView.design({
			anchorLocation: M.RIGHT,
			value: 'create',
			events: {
				tap: {
					target: Mtest.UserFormController,
					action: 'create'
				}
			}
		}),
		label: M.LabelView.design({
			anchorLocation: M.CENTER,
			value: 'Users'
		}),
		
		anchorLocation: M.TOP
	}),

	content: M.ListView.design({
		listItemTemplateView: Mtest.UserListItemView,
		contentBinding: {
			target: Mtest.UserListController,
			property: 'users'
		},
		
		idName: '_id'
	}),

	footer: M.ToolbarView.design({
			anchorLocation: M.BOTTOM,
			value: 'Footer'			
	})
});
