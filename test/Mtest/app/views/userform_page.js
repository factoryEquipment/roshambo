// ==========================================================================
// The M-Project - Mobile HTML5 Application Framework
// Generated with: Espresso 
//
// Project: Mtest
// View: userform_page
// ==========================================================================

Mtest.UserFormPage = M.PageView.design({

	childViews: 'header content footer',

	header: M.ToolbarView.design({
		childViews: 'back label challenge',

		back: M.ButtonView.design({
			anchorLocation: M.LEFT,
			value: 'Back',
         icon: 'arrow-l',
         internalEvents: {
         	tap: {
            		action: function() {
						Mtest.UserListController.mode = 'list';//we are not choosing to challenge anymore
               	history.back(-1);
            	}
         }}			
		}),		

		label: M.LabelView.design({
			anchorLocation: M.CENTER,
			value: 'User'
		}),
		
		challenge: M.ButtonView.design({
			anchorLocation: M.RIGHT,
			value: 'Challenge!!',
			events: {
				tap: {
					target: Mtest.UserListController,
					action: 'challenge'
				}
			}
		}),
		
		anchorLocation: M.TOP
		
	}),

	content: M.ScrollView.design({
		childViews: 'form formbuttons',
		
		form: M.FormView.design({
			showAlertDialogOnError: YES,
			childViews: 'ID telnum IMEI fname lname',
			
			ID: M.TextFieldView.design({
				label: 'ID',
				// initialText: 'required',
				// cssClassOnInit: 'initialText'
				disable: YES,
				disabled: YES,
				contentBinding: {
					target: Mtest.UserFormController,
					property: 'user._id'
				},
				contentBindingReverse: {
					target: Mtest.UserFormController,
					property: 'user._id'
				}
			}),

			telnum: M.TextFieldView.design({
				label: 'Cell Number',
				// initialText: 'required',
				// cssClassOnInit: 'initialText'
				contentBinding: {
					target: Mtest.UserFormController,
					property: 'user.telnum'
				},
				contentBindingReverse: {
					target: Mtest.UserFormController,
					property: 'user.telnum'
				}
			}),
			IMEI: M.TextFieldView.design({
				label: 'IMEI',
				contentBinding: {
					target: Mtest.UserFormController,
					property: 'user.IMEI'
				},
				contentBindingReverse: {
					target: Mtest.UserFormController,
					property: 'user.IMEI'
				}
			}),
			fname: M.TextFieldView.design({
				label: 'First Name',
				contentBinding: {
					target: Mtest.UserFormController,
					property: 'user.fname'
				},
				contentBindingReverse: {
					target: Mtest.UserFormController,
					property: 'user.fname'
				}
			}),
			lname: M.TextFieldView.design({
				label: 'Last Name',
				contentBinding: {
					target: Mtest.UserFormController,
					property: 'user.lname'
				},
				contentBindingReverse: {
					target: Mtest.UserFormController,
					property: 'user.lname'
				}
				
			})
			
		}),
		
		formbuttons: M.GridView.design({
          childViews: 'clearButton saveButton',

          layout: M.TWO_COLUMNS,

          clearButton: M.ButtonView.design({
              value: 'Clear',
              events: {
                  tap: {
                      target: Mtest.UserFormController,
                      action: 'clearForm'
                  }
              }
          }),

          saveButton: M.ButtonView.design({
              value: 'Save',
              events : {
                  tap: {
                      target: Mtest.UserFormController,
                      action: 'save'
                  }
              }
          })
      })
    }),

	footer: M.ToolbarView.design({
		value: 'FOOTER',
		isFixed: YES,
		anchorLocation: M.BOTTOM
	})

});

