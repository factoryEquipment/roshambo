// ==========================================================================
// The M-Project - Mobile HTML5 Application Framework
// Generated with: Espresso 
//
// Project: Mtest
// View: challenge_page
// ==========================================================================

Mtest.ChallengePage = M.PageView.design({


    childViews: 'header content footer',

   header: M.ToolbarView.design({
		childViews: 'back label',

		back: M.ButtonView.design({
			anchorLocation: M.LEFT,
			value: 'Back',
         icon: 'arrow-l',
         internalEvents: {
         	tap: {
            		action: function() {
               	history.back(-1);
            	}
         }}			
		}),		

		label: M.LabelView.design({
			anchorLocation: M.CENTER,
			value: 'Pick Move'
		}),
		
		anchorLocation: M.TOP
	}),

	/*
		Rock paper or scissor!
	*/
   content: M.ScrollView.design({
      childViews: 'choices',
		
		choices: M.ButtonGroupView.design({
			direction: M.VERTICAL,
			childViews: 'rock paper scissor',
			isCompact: NO,
			
			events: {
				change: {
					target: Mtest.ChallengeController,
					action: 'choose'
				}
			
			},
			
			rock: M.ButtonView.design({
				value: 'Rock'
			}),
			paper: M.ButtonView.design({
				value: 'Paper'
			}),
			scissor: M.ButtonView.design({
				value: 'Scissor'
			})
		})
   }),

   footer: M.ToolbarView.design({
       value: 'FOOTER',
       anchorLocation: M.BOTTOM
   })

});

