// ==========================================================================
// The M-Project - Mobile HTML5 Application Framework
// Generated with: Espresso 
//
// Project: Mtest
// View: dashboard_page
// ==========================================================================

Mtest.DashboardPage = M.PageView.design({

    /* Use the 'events' property to bind events like 'pageshow' */
    events: {
        pageshow: {
            target: Mtest.DashboardController,
            action: 'init'
        }
    },

    childViews: 'header content footer',

    header: M.ToolbarView.design({
        value: 'just for the hell of it',
        anchorLocation: M.TOP
    }),

    content: M.DashboardView.design({
        itemsPerLine : 2,
        isEditable: NO,
        contentBinding: {
            target: Mtest.DashboardController,
            property: 'items'
        }
    }),


    footer: M.ToolbarView.design({
        value: 'another just for the hell of it',
        anchorLocation: M.BOTTOM
    })

});

