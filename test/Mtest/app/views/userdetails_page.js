// ==========================================================================
// The M-Project - Mobile HTML5 Application Framework
// Generated with: Espresso 
//
// Project: Mtest
// View: userdetails_page
// ==========================================================================

Mtest.UserDetailsPage = M.PageView.design({
	

	/* Use the 'events' property to bind events like 'pageshow' */
	events: {
		pageshow: {
			target: Mtest.MyController,
			action: 'init'
		}
	},

	childViews: 'header content footer',

	header: M.ToolbarView.design({
		value: 'HEADER',
		anchorLocation: M.TOP
	}),

	content: M.ScrollView.design({
		childViews: 'label',
		label: M.LabelView.design({
			value: 'userdetails_page'
		})
	}),

	footer: M.ToolbarView.design({
		value: 'FOOTER',
		anchorLocation: M.BOTTOM
	})

});

