// ==========================================================================
// The M-Project - Mobile HTML5 Application Framework
// Generated with: Espresso 
//
// Project: Mtest
// View: userlist_item
// ==========================================================================

Mtest.UserListItemView = M.ListItemView.design({

   childViews : 'telnum IMEI',
	telnum: M.LabelView.design({
		valuePattern: '<%= telnum %>',
	})	,
	
	IMEI: M.LabelView.design({
		valuePattern: '<%= IMEI %>'
	})	,
		
	events: {
		tap: {
			target: Mtest.UserListController,
			action: 'userItemClicked'
		}
	}
});

