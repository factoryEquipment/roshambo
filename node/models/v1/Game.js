// 
//  Game.js
//  node
//  
//  Created by Sergio Bernales on 2012-02-17.
//  Copyright 2012 Locomoti LLC. All rights reserved.
// 

var mongoose = require('mongoose')
  , Schema = mongoose.Schema
  , utils = require('../../utils.js');


// ======================
// = Schema Definitions =
// ======================
var Match = new Schema({}, {strict: true});
Match.add({
	matchNum :{
		type: Number,
		default: 1
	},
	chaMove :{
		type: Number,
		default: -1,
						// 1 , 2 , 4
		enum: [-1, utils.COWBOY_MOVE, utils.BEAR_MOVE, utils.NINJA_MOVE]
	},
	oppMove  :{
		type: Number,
		default: -1,
						// 1 , 2 , 4
		enum: [-1, utils.COWBOY_MOVE, utils.BEAR_MOVE, utils.NINJA_MOVE]
	},
	strip :{
		type: String
	},
	
	/* Normal things */
	created :{
		type: Number,
		"default": Math.round((new Date()).getTime() / 1000)
	},
   	updated :{
		type: Number,
		"default": Math.round((new Date()).getTime() / 1000) 
	}
});

exports.MatchModel = mongoose.model('Match', Match);

//TODO: what if a game is canceled
var Game = new Schema({}, { strict: true });

Game.add({
	
	/* Game info */
	stakes:{
		type: String,
		required: true
	},
	/* User Info */
	chaID	:{ // challenger id TODO: make Objectid
		type: String
	},
	oppID:{ // opponent id
		type: String
	},
	winner: {
		type: String
	},
	
	matches: [Match], //TODO: i believe you can get length of this
	/* Normal things */
	created :{
		type: Number,
		"default": Math.round((new Date()).getTime() / 1000)
	},
   	updated :{
		type: Number,
		"default": Math.round((new Date()).getTime() / 1000) 
	}
});

// ==========================
// = Copy from passed game  =
// ==========================
Game.methods.copy = function copy(g){
	for(var key in g){
		console.log(g[key]);
		this[key]	= g[key];
	};
};

// ===================================================
// = Creates a new game with a default initial match =
// ===================================================
Game.statics.createNewGame = function search(gameInfo, cb) {
	debugger;
	var newGame = new Game(gameInfo);
	newGame.cid = gameInfo.userID;
	
	var newMatch = new Match({cmove: gameInfo.cmove});
	newGame.matches.push(newMatch);
	
};

// ==========================================================
// = Add a new match to the game (maybe last one was a tie) =
// ==========================================================
Game.methods.addNewMatch = function addNewMatch(){
	
	// we give the new match the hieght match num
	var _matchNum = this.matches.length+1;
	this.matches.push({matchnum: _matchNum});
}

// ======================================
// = Middleware run RIGHT AFTER saving  =
// ======================================
//TODO: we need to move this outside PRONTO
/*Game.post('save', function(err, next){
	if (err){
		console.log('we have encountered error');
		console.log(err);
		next();
	}
	console.log('Saved do some logic');
	console.log('this IS ITTTT!!!');
	console.log(this);

	//grab the last match
	var lastMatch = this.matches[gameDoc.matches.length-1];
	
	//someone still has to move, do nothing
	if(lastMatch.chaMove.valueOf() === -1 || lastMatch.oppMove.valueOf() === -1)
		next();
	
	console.log('WE HAVE A FINSIHED MATTCCHHH');		
	// success: determine further actions (an async call)
	GamesController.processFinishedGame(this);

	next();
});*/

exports.GameModel = mongoose.model('Game', Game);
/*
Game.pre('save', function(err, next){   

	if(next != null){
		console.log('NEXT NOT NULL');
		next();
	}
})*/
