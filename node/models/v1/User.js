// 
//  User.js
//  node
//  
//  Created by Sergio Bernales on 2012-02-17.
//  Copyright 2012 Locotmoti. All rights reserved.
// 

var mongoose = require('mongoose')
  , Schema = mongoose.Schema;

// =====================
// = Schema Definition =
// =====================
var User = new Schema({}, { strict: true });
User.add({
   imei	:{
		type: String,
		//index: true,
		required: false
	},
	telnum	:{
		type: Number,
		//index: true ,
		required: false
	},
	fname  	:{
		type: String,
		lowercase: true
	},
   lname  	:{
		type: String,
		lowercase: true
	},
   email   :{
		type: String,
		lowercase: true,
        required: true

	},
   username :{
		type: String,
		lowercase: true,
      required: true
	},
	
	/** Device push notification id's*/
	c2dmID 	:{
		type: String,
	},
	
	/* Phone info */
	os: {
		type: String,
		lowercase: true
	},
	model: {
		type: String
	},
	carrier: {
		type: String,
		lowercase: true
	},
	
	/* Normal things */
	created :{
		type: Number,
		"default": Math.round((new Date()).getTime() / 1000)
	},
   	updated :{
		type: Number,
		"default": Math.round((new Date()).getTime() / 1000) 
	}
});

// ===================
// = Copy attributes =
// ===================
User.methods.copy = function copy(s){
	for(var key in s){
		this[key]	= s[key];
	};
};

User.statics.searchByImei = function searchByImei(_imei, cb){
	return this.find({
		imei: _imei
	});
};

User.statics.searchByTelnum = function searchByTelnum(_telnum, cb){
    console.log('DEBUGG: ' + _telnum);
	return this.findOne({ telnum: _telnum}, function (err, doc){
        console.log('trying to find one');
        if(err){
            console.log('error');
        }

        if(doc == null )
            console.log('null');
        cb(doc);
    });
};


User.statics.searchByEmail = function searchByEmail(_email, cb){
	return this.find({
		email: _email
	});
};

exports.UserModel = mongoose.model('User', User);
