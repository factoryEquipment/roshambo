// 
//  Strip.js
//  node
//  
//  Created by serg on 2012-04-25.
//  Copyright 2012 Locomoti. All rights reserved.
// 

/*
	List of Comic Strips for game results
*/

var mongoose = require('mongoose')
  , Schema = mongoose.Schema
  , utils = require('../../utils.js');

// ======================
// = Schema Definitions =
// ======================
var Strip = new Schema({}, {strict: true});
Strip.add({
	scenario :{
		type: Number,
		required: true
	},
	tieMove :{
		type: Number,
		default: -1,
						// 1 , 2 , 4
		enum: [-1, utils.COWBOY_MOVE, utils.BEAR_MOVE, utils.NINJA_MOVE]
	},
	strip :{
		type: String,
		required: true
	},
	
	/* Normal things */
	created :{
		type: Number,
		"default": Math.round((new Date()).getTime() / 1000)
	},
   updated :{
		type: Number,
		"default": Math.round((new Date()).getTime() / 1000) 
	}
});

var stripModel = exports.StripModel = mongoose.model('Strip', Strip);


// ===============================================
// = populates the strip db all the comic strips  =
// = that exist for all our game scenario results =
// ===============================================
exports.populateStrips = function(){
	
	/** helper to create strip objects */
	var createStripObj = function(_scenario, _tieMove, _strip){
		return {
			scenario: _scenario,
			tieMove: _tieMove,
			strip: _strip
		};
	}
	
	/** helper to receive call back of saved strip record*/
	var dummySaveFunc = function(err, doc){ console.log(err); }

	/** ties */
	stripModel.create( createStripObj(utils.TIE, utils.COWBOY_MOVE, 'tie_cowboy_1'), dummySaveFunc);
	stripModel.create( createStripObj(utils.TIE, utils.BEAR_MOVE, 'tie_bear_1'), dummySaveFunc);
	stripModel.create( createStripObj(utils.TIE, utils.NINJA_MOVE, 'tie_ninja_1'), dummySaveFunc);
		stripModel.create( createStripObj(utils.WIN_COWBOY, null, 'win_cowboy1'), dummySaveFunc);
}






