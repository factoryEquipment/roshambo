// 
//  utils.js
//  node
//  
//  Created by serg on 2012-04-11.
//  Copyright 2012 Locomoti llc. All rights reserved.
// 

/**
* Useful Constants and methods for Roshambo
*/

// =====================================
// = Game Specific Rules and Constants =
// =====================================

/*
	cowboy beats bear,
	bear beats ninja,
	ninja beats cowboy.
*/
var cowboyMove = exports.COWBOY_MOVE = 1;
var bearMove	= exports.BEAR_MOVE = 2;
var ninjaMove	= exports.NINJA_MOVE = 4;

// results:
exports.TIE				= cowboyMove ^ cowboyMove;//could have been calculated with any
exports.WIN_COWBOY	= cowboyMove ^ bearMove;
exports.WIN_BEAR 		= bearMove ^ ninjaMove;
exports.WIN_NINJA 	= ninjaMove ^ cowboyMove;


// ===============
// = User States =
// ===============

// user Does Not Exist
exports.USER_STATE_DNE = 0

/**
user does exist but has never registered
this can happen if phone number has been challenged before
but owner of phone number has never registered as a user,
the owner never downloaded the app! */
exports.USER_STATE_UNREG = 1;


/**
User is registered. Means they have a username and also
should probably definitley have a registration id crom 
the c2dm google server
*/
exports.USER_STATE_REG = 2;


// =================================================
// = Determines what state the user record is in.  =
// = see above for list of states and descriptions =
// =================================================
exports.getUserState = function(user){
 	if(user == null)
		return this.USER_STATE_DNE;
	
	// no username means never registered
	if(user.username == null)
		return this.USER_STATE_UNREG;
		
	// user checks out	
	return this.USER_STATE_REG;
};


