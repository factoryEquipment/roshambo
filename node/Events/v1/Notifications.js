// 
//  Notifications.js
//  Roshamboo
//  
//  Created by serg on 2012-03-05.
//  Copyright 2012 Locomoti. All rights reserved.
// 

/*
	Defines events to notify players in a game of a win, tie, or new challenge.
*/


var EventEmitter = require('events').EventEmitter,
	util = require('util');


// =============================================
// = Constructor for Notification EventEmitter =
// =============================================	
var Notifications = function (){
	// set up event emitter
	EventEmitter.call(this); 
	
}

// so that we can emmit stuff
util.inherits(Notifications, EventEmitter);

// ================================================================
// = A new challenge has been created, we need to notify opponent =
// ================================================================
Notifications.prototype.newChallenge = function newChallenge (game, opp) {
	this.emit('challenge', game, opp)
}


// ====================================
// = Game ended, someone won. NOtify! =
// ====================================
Notifications.prototype.win = function win (game){
	this.emit('win', game)
}

// =================================
// = Tie, game keeps going NOTIFY! =
// =================================
Notifications.prototype.tie = function tie (game) {
	this.emit('tie', game)
}



module.exports = new Notifications();