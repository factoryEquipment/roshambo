// 
//  app.js
//  node
//  
//  Created by Sergio Bernales on 2012-02-17.
//  Copyright 2012 Locomoti LLC. All rights reserved.
// 

/*
	TODO: User should have 10 games created by default. They can user it whenever.
*/

// =======================
// = Module Dependencide =
// =======================
var express 	= require('express'),
	routes 		= require('./routes'),
	mongoose 	= require('mongoose'),
	validators	= require('./middleware/validators.js'),
	fs = require('fs'),
	// to register notification calls backs
	notifController = require('./controllers/v1/NotificationsController.js'),
	c2dmController = require('./controllers/v1/C2DMController.js');
	

// https options: key and certificate
var options = {
  key: fs.readFileSync('./ssl/test/test.key'),
  cert: fs.readFileSync('./ssl/test/test.cert')
};	
	
// create the new HTTPS server
var app = module.exports = express.createServer(options);

// =================
// = Configuration =
// =================
app.configure(function(){
  app.set('views', __dirname + '/views');
  app.set('view engine', 'jade');
  app.set('view options', {layout:false});
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(app.router);
  app.use(express.static(__dirname + '/public'));
});

app.configure('development', function(){
  app.use(express.errorHandler({ dumpExceptions: true, showStack: true })); 
});

app.configure('production', function(){
  app.use(express.errorHandler()); 
});

// ============
// = Database =
// ============

// mongodb://nodejitsu:1e48a7686f33ee2f17fb3dd5747e02d9@staff.mongohq.com:10066/nodejitsudb358791417480
mongoose.connect('mongodb://localhost/local_db', function(err){
// mongoose.connect('mongodb://nodejitsu:1e48a7686f33ee2f17fb3dd5747e02d9@staff.mongohq.com:10066/nodejitsudb358791417480', function(err){
	if(err)
		console.log("could not connect to the server");
});
require('./models/v1/User');
require('./models/v1/Game');
var strip = require('./models/v1/Strip');
strip.populateStrips();

// ==========
// = Routes =
// ==========
app.get('/', routes.index);

// user creation/interaction
app.post('/v1/users', 			validators.jsonCreateUser, routes.users.create); //create user
app.put('/v1/users/:userID', 	routes.users.update); //update user info
app.get('/v1/users/:userID', 	routes.users.show); //get user info
app.get('/v1/users', 			routes.users.list); //for debugging
//app.delete('/v1/users/id'), 	routes.index); //delete user info

// game creation/interaction
app.post('/v1/users/:userID/games', 			validators.jsonCreateGame, routes.games.create); //create a new game for user (should already include first move)
app.get('/v1/users/:userID/games',				routes.games.list); //get all games for given user
app.get('/v1/users/:userID/games/:gameID', 	routes.games.show);	//get the current game info
app.put('/v1/users/:userID/games/:gameID', 	routes.games.update);//update the given name

// match creation interaction
app.post('/v1/games/:gameID/matches', 						validators.jsonCreateMatch, routes.matches.create); //create a new match
app.put('/v1/users/:userID/games/:gameID/matches', 	validators.jsonUpdateMatch, routes.matches.update); //play

//I dont thin the bottome two are nexessary, when would we need exact math retrieaval? 
//and when we want all the mathes, just get the freaking whole game
// app.get('/v1/games/:gameID/matches/:matchID',	routes.matches.show); //get match info
// app.get('/v1/games/:gameID/matches', 				routes.matches.list); //the the whole list of matches or this game



var port = process.env.PORT || 3000;
app.listen(port, function() {
	console.log("Express server listening on port %d in %s mode", app.address().port, JSON.stringify(app.settings));
});



// =====================================================
// = Error Handling TODO: maybe put in a seperate file =
// =====================================================
process.on( "uncaughtException", function(  err ){
	console.log("There has been an uncaught exception:");
	console.log(err.name + ": " + err.msg + err.stack);
	
	var next = err.n;
	next(new Error(err.name + ": " + err.msg));	
	
	// var res = err.r;
	// res.render('500', {user: {telNUm: "12321dddd"}});		
	// res.end();
});

app.error(function(err, req, res, next){
	console.log("throwin error!!!");
	res.render('500.jade');
});


c2dmController.testPushNewChallenge() ;
