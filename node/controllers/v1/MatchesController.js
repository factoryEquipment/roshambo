// 
//  MatchesController.js
//  Roshamboo
//  
//  Created by Sergio Bernales on 2012-03-02.
//  Copyright 2012 Locomoti. All rights reserved.
// 

var mongoose = require('mongoose');
var GamesController = require('./GamesController');


// =============================================================
// = Create a new match for a given game ID =
// =============================================================
exports.create = function(req, res, next){
		var matchModel = mongoose.model('Match');
		var gameModel = mongoose.model('Game');

		// first find the game
		matchModel.findById(req.params.gameID, function(err, gameDoc){
			if(err){
				next(new Error('Game not found'));
				return;
			}
			
			// var newMatch = new matchModel(reqGame.match);
			// add new match
			//TODO: update the game to refelect a new game was added
			var numMatches = gameDoc.matches.push(reqGame.match);
			gameDoc.save(function(err, savedG){
				if(err){
					next(new Error('Could not create a new match'));
					return;
				}
				
				//TODO: this is where we notify that a new match is pending (game not over)
				res.send(numMatches);
				res.end();
			});
			
		});
};

// ======================================================================
// = Update a match. Basically, user made a move								=
// ======================================================================
exports.update = function(req, res, next){
	var gameModel = mongoose.model('Game');
	
	var match = req.body.match;
	
	//the player who submitted this move [cmove, omove]
	var move = match.move;
	var playerID = match.userNodeID;
	var gameID = match.gameNodeID;
		
	// id we need to query as [chaID or oppID]
	// var whoseID = whoseMove === 'chaMove' ? 'chaID' : 'oppID';
	
	// console.log(whoseMove + ":::" + playerID  + ":::" + gameID + ":::" + whoseID + ":::" + req.body.match[whoseMove]);	
	
	//create the query, makeing sure this game exists
	console.log('creating a query for:'+playerID.toString());
	
	//var q = { _id: mongoose.Types.ObjectId(gameID), $or: [{chaID: playerID} ,{oppID:playerID}]    }
	
	 var q = gameModel.where( '_id', gameID)// with the given game id
	 		  .or([{ chaID: playerID }, { oppID: playerID }]); //one of the id's belong to the game
						// .where(whoseID, playerID)// playerID correctly opp or chall
						// .where('matches.'+whoseMove, -1);// player hasn't played yet
					
	gameModel.findOne(q, function(err, gameDoc){
		if (err || gameDoc == null){
			next(new Error('Not a valid game!'));	
			return;
		}

		console.log('found one!: ' + gameDoc);
		
		// determine if submitting player is the opponent or challenger move
		var whoseMove = playerID === gameDoc.chaID ? 'chaMove' : 'oppMove';
		
		// get the latest match
		var lastMatch = gameDoc.matches[gameDoc.matches.length-1];
		
		if(lastMatch[whoseMove].valueOf() === -1 ){//player has not played yet
			// add player's move!
			lastMatch[whoseMove] = move;
			
			gameDoc.save(function(err, savedGameDoc){
				if (err){
					next(new Error('could not save'));	
					return;
				}

				// check if the game is finished
				if(lastMatch.chaMove.valueOf() !== -1 || lastMatch.oppMove.valueOf() !== -1){
					console.log('GAME FINISHED!!!!! ');
					GamesController.processFinishedGame(savedGameDoc);
				}
				
				
				// success: send back to user
				res.send(JSON.stringify({game: savedGameDoc}));
				res.end();
				// 
				// // success: determine further actions (an async call)
				// GamesController.determineGameEventsAfterPlay(savedG);
			});
		}
		else{//error, player had already played!
			next(new Error(playerID + ' already played'));
		}
		
	});
};
