// 
//  C2DMController.js
//  node
//  
//  Created by serg on 2012-04-12.
//  Copyright 2012 Locomoti llc. All rights reserved.
// 

/**
In charge of doing the Androd push notifications
*/

var C2DM = require('c2dm').C2DM;
var mongoose = require('mongoose');
var rutils = require('../../utils.js');

// var TOKEN_CHALLENGE = 'challenge';
// var TOKEN_TIE = 'tie';
// var TOKEN_LOSE = 'lose';
// var TOKEN_WIN = 'win';

var TOKEN_CHALLENGE = '0';
var TOKEN_TIE = '1';
var TOKEN_LOSE = '2';
var TOKEN_WIN = '3';

/*
	Configure C2DM for continuous usage
*/
var config = {
	keepAlive: true, // we will use this a LOT!
	token: 'Auth=DQAAAMYAAAB99WulNyormXn_RSiKq3v-ZcZOMV5DAimjxUBpajGvH2V_wukI5V0P6kZHm4jBxDSxVjET4cDU6hQLLRTENrIORgzSxtOSrV2suqqZP6VK0p3kH5n4V5LOCGiIiz1XuJ2aUXnncoQ35SLgWOY6Ra3c9LcmZigv-gDoYFyk-267rj-kn49GxzD-qDkmt7kUfj7YqXhvFp86VixfTO-UdbO6T77vPotXj3xLxOsKT3m6DcRkN4ybqOD53W2aVNm1d_gtGY1m0NXms09xVBN2rciE',  // N.B. include with Auth= prefix

 };
 var c2dm = new C2DM(config);


// ===========================================================
// = Build a C2DM message that will be sent to google server =
// ===========================================================
var buildC2DMMessage = function(game, user){
 	return {
		registration_id: 'APA91bFiIPMYW4xsPoGq5Xp_esMWJn4KTmEF4D0GmacaFrVYRIICTHE9Z3cJhp69S5rcKzkRHNMOf1GRDjoFu32MZQLoJxvB2svgzY8Y5YrcXsuJx67r0Y8apAST6VPNtQ7hLywlM859FbCuPS4Co2LjUfQLRocbRsTxQo26OrnUQiuKS7viYXg',
		collapse_key: 'Multiple',
		'data.key1':  'game.stakes',
		'data.key2': 'sergio'	//todo: get the id of the challenger
	};
};

// ===============================================
// = Push a new challenge message to an opponent =
// ===============================================
exports.pushNewChallenge = function(game, opp){
	
	// get the challenger info (we need their username)
	var userModel = mongoose.model('User');
	userModel.findById(game.chaID, function(err, chaDoc){
		if (err){
			console.log('Error couldnt send notification cause challenger not found')
			console.log(game)
			return;
		}
	
	console.log(opp);
	
		// build message New Challenge c2dm mesage
		var message =  {
			registration_id: opp.c2dmID.toString(),
			collapse_key: 'Fight for your way!',
			
			'data.token': TOKEN_CHALLENGE,
			'data._id': game._id.toString(),
			'data.chaID' : game.chaID,
			'data.oppID' : game.oppID,
			'data.chaUsername'  : chaDoc.username,
			'data.created'		: game.created.toString(),
			'data.stakes':  game.stakes.toString(),

			/** TODO: testing. do something about this*/
			'data.game': JSON.stringify(game),
		};
		
		console.log(message);
		// send message
		c2dm.send(message, function(err, messageId){
			console.log(err);
			console.log(messageId);
		});
	});
}

// ================================================
// = Game was a tie, we have to pusgh the results =
// ================================================
exports.pushTie = function(game){
	var userModel = mongoose.model('User');
	
	// get both user records
	var usersQuery = userModel.$in('_id', [game.chaID, game.oppID]);
	
	userModel.find(usersQuery, function(err, userDocs){
		if(err || userDocs.length != 2)
			return;
		
		
		// the message to be sent
		// message to show tie
		var message =  {
			registration_id: userDocs[0].c2dmID.toString(),
			collapse_key: 'Results are in!',

			'data.token': TOKEN_TIE,
			'data._id': game._id.toString(),
			'data.chaID' : game.chaID,
			'data.oppID' : game.oppID,
			'data.chaUsername'  : chaDoc.username,
			'data.oppUsername'  : chaDoc.username,
			'data.created'		: game.created,
			'data.stakes':  game.stakes.toString(),

			/** TODO: testing. do something about this*/
			'data.game': JSON.stringify(game),
		};
		
		// send message to first user
		c2dm.send(message, function(err, messageId){
			console.log(messageId);
			
			// we reuse the same message, 
			// now send it to the other user
			message.registration_id = userDocs[1].c2dmID.toString()
			
			c2dm.send(message, function(err, messageId){
				console.log(messageId + ' : Both message sent, its all over now.');
			});
		});
	});
}

// ==================================================
// = Somebody won the game, we need to push results =
// ==================================================
exports.pushWin = function(game){
	// precondition: a winner should have been set already
	if(game.winner == null)
		return;
	
	
	var userModel = mongoose.model('User');
	
	// get both user records
	var usersQuery = userModel.$in('_id', [game.chaID, game.oppID]);
	
	userModel.find(usersQuery, function(err, userDocs){
		if(err || userDocs.length != 2)
			return;
		
		// determine winner, array index corresponds to userDocs index
		var userTokens = [TOKEN_LOSE, TOKEN_LOSE];
		if(game.winner === userDocs[0]._id)
			userTokens[0] = TOKEN_WIN;
		else
		 	userTokens[1] = TOKEN_WIN;
		
		// the message to be sent
		// message to show tie
		var message =  {
			registration_id: userDocs[0].c2dmID.toString(),
			collapse_key: 'Results are in!',

			'data.token': userTokens[0],
			'data._id': game._id.toString(),
			'data.chaID' : game.chaID,
			'data.oppID' : game.oppID,
			'data.chaUsername'  : chaDoc.username,
			'data.oppUsername'  : chaDoc.username,
			'data.created'		: game.created,
			'data.stakes':  game.stakes.toString()
		};
		
		// send message to first user
		c2dm.send(message, function(err, messageId){
			console.log(messageId);
			
			// we reuse the message sent and just update the token
			message['data.token'] = userTokens[1];
			
			// now send it to the other user
			message.registration_id = userDocs[1].c2dmID.toString()
			c2dm.send(message, function(err, messageId){
				console.log(messageId + ' : Both message sent, its all over now.');
			});
		});
	});
}

exports.testPushNewChallenge = function(){
	// build message New Challenge c2dm mesage
	var message =  {
		registration_id: 'APA91bFiIPMYW4xsPoGq5Xp_esMWJn4KTmEF4D0GmacaFrVYRIICTHE9Z3cJhp69S5rcKzkRHNMOf1GRDjoFu32MZQLoJxvB2svgzY8Y5YrcXsuJx67r0Y8apAST6VPNtQ7hLywlM859FbCuPS4Co2LjUfQLRocbRsTxQo26OrnUQiuKS7viYXg',
		collapse_key: 'Fight for your way!',
		'data.token': 'challenge',
		'data.stakes':  "for everything in the world",
		'data.gameid': 'game._id:chaDoc.username',
		'data.chausername' : 'sergio'
	};
	
	console.log(message);
	// send message
	c2dm.send(message, function(err, messageId){
		console.log(err);
		console.log(messageId);
	});

}