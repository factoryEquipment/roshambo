// 
//  NotificationsController.js
//  Roshamboo
//  
//  Created by serg on 2012-03-05.
//  Copyright 2012 Locomoti. All rights reserved.
// 

/*
	Here we define instantiate an Notifications Event Emitter object
	and define functions in charge of 
	hadnling those events.	
*/

var mongoose = require('mongoose');
var notiEmitter = require('../../Events/v1/Notifications.js');
var c2dmController = require('./C2DMController.js');

// ==============================================================================
// = Send notifications to both users there there was a tie, need to play again =
// ==============================================================================
notiEmitter.on('tie', function(game){
	console.log('TIE!!')
	
	c2dmController.pushTie(game);
});


// ========================================================
// = Send notifications to loser and winner appropriatley =
// ========================================================
notiEmitter.on('win', function(game){
	console.log('win!')
	
	c2dmController.pushWin(game);
});


// ================================================
// = New Challenge, notify the challenged player! =
// ================================================
notiEmitter.on('challenge', function(game, opp){
	console.log('CHALLENGE!!')
	
	// TODO: should be updated to handle any type of opponent
	sendAndroidChallenge(game, opp);
	
	
});

// ==============================================================
// = Sends a challenge to an android device. This means sending
// = A Cloud 2 Device Message (c2dm) notifying the device a 
// = challenge has been created
// ==============================================================
function sendAndroidChallenge(game, opp) {
	console.log("Challengeing: " + opp);
	
	c2dmController.pushNewChallenge(game,opp) ;//TODO send callback to receive message ID
}




