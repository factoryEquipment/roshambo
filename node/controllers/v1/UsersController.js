// 
//  UsersController.js
//  node
//  
//  Created by Sergio Bernales on 2012-02-17.
//  Copyright 2012 Locomoti LLC. All rights reserved.
// 

var mongoose = require('mongoose');

// ==================================
// = Create a new user. Async call. =
// ==================================
exports.create = function(req, res, next){
		//TODO: is this ok to be here? Why not make it global(it doesnt work)
		var userModel = mongoose.model('User');
        	
		var newUser = new userModel(req.body.user);
        console.log(newUser);
		newUser.save( function(err, uDoc){
			if (err){
                console.log(err);
				next(new Error('Could not save new user!'));	
				return;
			}
			
			res.send(JSON.stringify( { user: uDoc} ));
			console.log(JSON.stringify( { user: uDoc} ));
			
			res.end();
		});
};

// =============================
// = Get user and send it back =
// =============================
exports.show = function(req, res, next){
	var userModel = mongoose.model('User');
	
	userModel.findById(req.params.userID, function(err, uDoc){
		if (err){
			throw {name: "DBERROR", msg: "Could not find user", r: res, n: next };
			return;
		}
		
		res.send(JSON.stringify({user: uDoc}));
		res.end();
	});
	
};

// ====================
// = Update user info =
// ====================
exports.update = function(req, res, next){
	var userModel = mongoose.model('User');
	
	userModel.findById(req.params.userID, function(err, uDoc){
		if (err){
			throw {name: "DBERROR", msg: "Could not find user", r: res, n: next };
			return;
		}

		uDoc.copy(req.body.user)
		uDoc.save(function(err, savedU){
			if (err){
	    
				throw {name: "DBERROR", msg: "Could not save", r: res, n: next };
				return;
			}
			
			res.send(JSON.stringify(savedU))
			res.end();
		});

	});
};


// ========================================
// = FOR DEBUGGING: List of all the users =
// ========================================
exports.list = function(req, res, next){
		var userModel = mongoose.model('User');	
		userModel.find({}, function(err, ulist){
			if (err){
				throw {name: "DBERROR", msg: "Could not find list of users", r: res, n: next };
				return;
			}
				
			res.send(JSON.stringify({users: ulist}));
			res.end();
	 });
};
