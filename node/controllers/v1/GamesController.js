// 
//  GamesController.js
//  node
//  
//  Created by Sergio Bernales on 2012-02-17.
//  Copyright 2012 Locomoti LLC. All rights reserved.
// 

var mongoose = require('mongoose');
var notiEmitter = require('../../Events/v1/Notifications.js');
var rutils = require('../../utils.js');
var stripsController = require('./StripsController.js');

// TODO: create the rules


// =====================================
// = Create a game for the given user. =
// = Basically create a challenge	   =
// =====================================
exports.create = function(req, res, next){
	//TODO: is this ok to be here? Why not make it global(it doesnt work)
	var gameModel = mongoose.model('Game');

	console.log('New game model creation: before');
	console.log(req.body.game)

	//new game
	var newGame = new gameModel(req.body.game);

	// some strange encoding happens hex string's toString() 
	// not explicitlty called
	newGame.chaID = req.body.game.chaID.toString();

	//new match 
	newGame.matches.push({chaMove: req.body.game.chaMove});

	// now we have to check if the opponenet exists
	// if it does then we add the opponenet id into
	// the json and the client will know it exists
	var userModel  = mongoose.model('User');

	userModel.searchByTelnum(req.body.game.oppTelnum, function(oppDoc){
		// determine what state user record is in
		var oppState = rutils.getUserState(oppDoc);

		// depending on opponent state, do something
		emitChallenge = false;
		switch(oppState) {
			case rutils.USER_STATE_DNE:
				console.log('does not exist');
				// nothing to do, we send back newGame doc
				// with emtpy Opp ID and device will notify
				// opp through text
				break;

			case rutils.USER_STATE_UNREG:
				console.log('Exists but unregistered');
				// nothing to do, we send back newGame doc
				// with emtpy Opp ID and device will notify
				// opp through text
				break;

			case rutils.USER_STATE_REG:
				console.log('oppent registered');
				// we will emit challenge
				emitChallenge = true;
				
				// also set opponent id
				newGame.oppID = oppDoc._id;
				break;

			default:
				next(new Error('Invalid user state!'));	
		}


		// now save the new game and send back to client
		newGame.save(function(err, gameDoc){
			if (err){
				console.log(err);
				next(new Error('Could not save game!'));	
				return;
			}
			
			// notification to opponent
			if (emitChallenge) {
				
				// since opp found, add their id to data
				newGame.oppID = oppDoc._id;
				//also add opponenet username
				newGame.set('oppUsername', oppDoc.username);
				// add game id 
				newGame.set('_id', gameDoc._id);
				// start notification process of a CHALLENGE!!!
				notiEmitter.newChallenge(newGame, oppDoc);
			}

			// add androidID to the response, helps
			// challenger device to figure out which game its receiving
			// a resonse about
			gameDoc.set('androidID', req.body.game.androidID);
			

			console.log(gameDoc);

			res.send(JSON.stringify({game: gameDoc}));
			res.end();
		});
	}); 
};

// =================================
// = Get info of a particular game =
// =================================
exports.show = function(req, res, next){
	var gameModel = mongoose.model('Game');
	
	gameModel.find({_id: req.params.gameID}, function(err, gDoc){		
		if (err){
			next(new Error('Could not find any games!'));	
			return;
		}

		res.send(JSON.stringify(gDoc));
		res.end();
	});
};

// ====================================
// = Get all games for the given user =
// ====================================
exports.list = function(req, res, next){
	var gameModel = mongoose.model('Game');
	
	var gameList = gameModel.find({}, function(err, glist){
		if (err){
			next(new Error('Could not list games!'));	
			return;
		}
			
		res.send(JSON.stringify(glist));
		res.end();
	});
};

// =================
// = Update a game =
// =================
exports.update = function(req, res, next){
	var gameModel = mongoose.model('Game');
	
	gameModel.findById(req.params.gameID, function(err, gDoc){
		if (err){
			next(new Error('Not a valid game!'));	
			return;
		}
			
		gDoc.copy(req.body.game);
		gDoc.save(function(err, savedG){
			if (err){
				next(new Error('could not save'));	
				return;
			}
			
			res.send(JSON.stringify(savedG));
			res.end();
		});
	});
};

// ==============================================================
// = Process a game that has moves from boths plater submitted  =
// = this involces saving notification stats and emitting the   =
// = notification events. 													 =
// ==============================================================		
exports.processFinishedGame = function(game){
	process.nextTick(function() {
		
		var winner = getWinner(game);
			
		if(winner === utils.TIE){
			//add a new match
			game.addNewMatch();
		}
		else{
			// somebody won, put it in our game doc
			game.winner = winner;
		}
		
		// get a random comic strip for the given scenario
		stripsController.getStrip(game.chaMove ^ game.oppMove, game.oppMove, function(randomStrip){
			if(randomStrip == null)
				return;
			
			// add random strip and save doc
			game.strip = randomStrip;
			game = game.save(function(err, savedG){
				if(err){
					throw new Error('Couldnt save the new game!')
				}

				// emit notification for whatever scenario we got
				if(winner === utils.TIE)	
					notiEmitter.tie(savedG);
				else
					notiEmitter.win(savedG);
			});
		});
		
		
	});
};

// ==========================================================================
// = returns the user id of the winner of the game. return 0 if it is a tie =
// ==========================================================================
var getWinner = function(game) {
	// get the latest match
	var lastMatch = game.matches[game.matches.length-1];
	
	// grab players moves
	var chaMove = lastMatch.chaMove.valueOf();
	var oppMove = lastMatch.oppMove.valueOf();
	
	// XOR the moves to determine outcome
	var scenario = chaMove ^ oppMove;
	
	switch(scenario) {
		case utils.TIE: // tie
			return utils.TIE;
			
		case utils.WIN_COWBOY: // bear vs cowboy. cowboy wins
			if(chaMove === utils.COWBOY_MOVE)
				return game.chaID;
			else
				return game.oppID;
				
		case utils.WIN_NINJA: // cowboy vs ninja. ninja wins
			if(chaMove === utils.NINJA_MOVE)
				return game.chaID;
			else
				return game.oppID;
		
		case utils.WIN_BEAR: // bear vs ninja. bear wins
			if(chaMove === utils.BEAR_MOVE)
				return game.chaID;
			else
				return game.oppID;
		default:
			return -1;
	}
}
