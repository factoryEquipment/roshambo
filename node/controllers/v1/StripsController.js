// 
//  Strip.js
//  node
//  
//  Created by serg on 2012-04-25.
//  Copyright 2012 Locomoti. All rights reserved.
// 

/*
	Does Business logic that has to do with the comic stripts
	that shows results to the user. Like choosing which strip to show for a 
	particular results.
*/

var mongoose = require('mongoose');
var rutils = require('../../utils.js');



// =============================================================
// = We are storing strips in array for now, later we may have =
// = to store them in mongo. 												=
// =============================================================



// =====================================================
// = Picks a random record from an array of documents. =
// =====================================================
var pickRandomStrip = function(err, strips) {
	if(err)
		console.log(err);
	
	
}

// =============================================================
// = Determines comic strip that client device should display  =
// = If result is a TIE then tieMove should hold the character =
// = the tie happened with.											   =
// =============================================================
exports.getStrip = function(_scenario, _tieMove, cb){
	var stripModel = mongoose.model('Strip');
		
	// query strips for scenario (the in is to support tie where it requires a _tieMove to be sent)
	stripModel.findAll({scenario:_scenario, tieMove: {$in: [null, _tieMove]}},	function(err, stripDocs){
		if(err)
			return cb(null);
			
		// get the size of the results to detemrin modulo
		var mod = stripsDocs.length-1;
		
		// pick a random index (random int is from 1-100)
		var i = Math.floor((Math.random()*100)+1) % mod;
		
		// return that random strip
		cb(stripDocs[i].strip);
	} );	
}