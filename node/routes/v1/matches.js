// 
//  matches.js
//  Roshamboo
//  
//  Created by Sergio Bernales on 2012-03-02.
//  Copyright 2012 Locomoti . All rights reserved.
// 

var mcontroller = require('../../controllers/v1/MatchesController');

// =====================================================
// = POST: start/create a new match for a given gameID =
// =====================================================
exports.create = function(req, res, next){
	console.log(req.method + ' : ' + req.url);	
	mcontroller.create(req, res, next);
  	//res.render('index', { title: 'Express' });
};

// ==========================
// = PUT: Update the match  =
// ==========================
exports.update = function(req, res, next){
	console.log(req.method + ' : ' + req.url);	
	mcontroller.update(req, res, next);
};

/* Not necessary: see app.js
// ===========================
// = GET: get the match info =
// ===========================
exports.show = function(req, res, next){
	console.log('show game');
	mcontroller.show(req, res, next);
};

// ====================================================
// = GET: get a list of matches for the given game ID =
// ====================================================
exports.list = function(req, res, next){
	console.log(req.method + ' : ' + req.url);	
	mcontroller.list(req, res, next);
};
*/
