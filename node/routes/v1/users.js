// 
//  users.js
//  node
//  
//  Created by Sergio Bernales on 2012-02-17.
//  Copyright 2012 Locomoti LLC. All rights reserved.
// 

var ucontroller = require('../../controllers/v1/UsersController');

// =========================
// = POST: create the user =
// =========================
exports.create = function(req, res, next){
	console.log(req.method + ' : ' + req.url);	
	ucontroller.create(req, res, next);
};

// =======================
// = GET: show user info =
// =======================
exports.show = function(req, res, next){
	console.log(req.method + ' : ' + req.url);	
	ucontroller.show(req, res, next);
};

// =========================
// = PUT: update user info =
// =========================
exports.update = function(req, res, next){
	console.log(req.method + ' : ' + req.url);	
	ucontroller.update(req, res, next);
};

// =======================
// = GET: List all users =
// =======================
exports.list = function(req, res, next){
	console.log(req.method + ' : ' + req.url);	
	ucontroller.list(req, res, next);
};