// 
//  games.js
//  node
//  
//  Created by Sergio Bernales on 2012-02-17.
//  Copyright 2012 Locomoti LLC. All rights reserved.
// 

var gcontroller = require('../../controllers/v1/GamesController');

// ===========================
// = POST: create a new game =
// ===========================
exports.create = function(req, res, next){
	console.log(req.method + ' : ' + req.url);	
	gcontroller.create(req, res, next);
  	//res.render('index', { title: 'Express' });
};

// ===========================
// = GET: get/show game info =
// ===========================
exports.show = function(req, res, next){
	console.log('show game');
	gcontroller.show(req, res, next);
};

// ========================================
// = GET: get/show list of games for user =
// ========================================
exports.list = function(req, res, next){
	console.log(req.method + ' : ' + req.url);	
	gcontroller.list(req, res, next);
};

// ====================
// = PUT: update game =
// ====================
exports.update = function(req, res, next){
	console.log(req.method + ' : ' + req.url);	
	gcontroller.update(req, res, next);
};