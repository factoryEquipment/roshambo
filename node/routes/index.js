// 
//  index.js
//  Roshamboo
//  
//  Created by Sergio Bernales on 2012-02-18.
//  Copyright 2012 Locomoti. All rights reserved.
// 

// ==============
// = Our routes =
// ==============
exports.users = require('./v1/users');
exports.games = require('./v1/games');
exports.matches = require('./v1/matches');



/*
 * GET home page.
 */
exports.index = function(req, res){
  res.render('index', { title: 'Express' })
};



