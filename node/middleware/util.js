// 
//  util.js
//  node
//  
//  Created by Sergio Bernales on 2012-02-17.
//  Copyright 2012 Locomoti LLC. All rights reserved.
// 


/*
	Deprecated: Realized that express will extract the 
	JSON for you automatically. 
*/
// ===============================================
// = Will extract the JSON body from the request =
// ===============================================
exports.extractJSON = function (req, res, next){
	console.log(req.method + ' : ' + req.url);	
	console.log(req.body);
	res.end();
	return;
	
	
	
	var reqJSON = '';	
	
	req.on('data', function(chunk){
		 console.log('receiving chunk: ' + chunk);
		reqJSON += chunk;
	});
	
	
	//when everything received, send to router
	req.on('end', function(){
		//TODO: tell mobile deivce to always have parenthesis around JSON
		req.jsonBody = eval(reqJSON);
		console.log(req.jsonBody)
		next();
	});
}