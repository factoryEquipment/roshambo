// 
//  validators.js
//  Roshamboo
//  
//  Created by Sergio Bernales on 2012-02-18.
//  Copyright 2012 Locomoti LLC. All rights reserved.
// 

// ==================================================
// = Make sure the JSON sent is formatted correctly =
// ==================================================

// TODO: create array of each input that we are supposed to expect from ther server

exports.jsonCreateUser = function(req, res, next){
	var jsonU = req.body.user;
    console.log(jsonU);
    console.log(jsonU.telnum);
    console.log(jsonU.IMEI);
	
    // old validation, imei, telnum not required, username, email is required
    //var missingFields = (jsonU == null || jsonU.telnum == null || jsonU.IMEI == null);
	var missingFields = (jsonU == null || 
								jsonU.username == null || 
								jsonU.email == null );//||
//								jsonU.c2dmID == null);
	
	if(missingFields)
		next(new Error('Invalid USER format'));	
	else
		next();
};


// ===============================================
// = Make sure the game info in JSON is complete =
// ===============================================
exports.jsonCreateGame = function(req, res, next){
	var jsonG = req.body.game;
	console.log(req.body)
	
	var missingFields = (jsonG == null || jsonG.chaID == null || jsonG.chaMove == null)
	
	if(missingFields)
		next(new Error('Invalid GAME format'));
	else
		next();
}


// ============================================
// = Make sure match info in JSON is complete =
// ============================================
exports.jsonUpdateMatch = function (req, res, next) {
	var jsonM = req.body.match;
	console.log(req.body)
	
	var missingFields = (jsonM == null) || (jsonM.move == null) ||
		(jsonM.userNodeID == null) ||  (jsonM.gameNodeID == null);

	if(missingFields)
		next(new Error('Invalid MATCH format!!'));
	else
		next();
}

exports.jsonCreateMatch = function jsonCreateMatch(req, res, next) {
	var jsonM = req.body.match;
	consol.log(req.body)
}

